'use strict';
// get dependencies
const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();
const app = express();

/*  CORS  */
const cors = require("cors");
const path = require('path');

// parse requests
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multipart = require('connect-multiparty');
app.use(multipart()) //Express 4

//Habilitar CORS para todos los metodos HTTP 
app.use(cors());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

  app.use(express.static("dist/angular8-template-app")); // Carpeta de los archivos estaticos de angular


 app.get('/*', function(req, res) {
  res.sendFile(path.join(__dirname+ '/dist/angular8-template-app' , 'index.html'))
});

app.get('/configuration/*', function(req, res) {
  res.sendFile(path.join(__dirname+ '/dist/angular8-template-app' , 'index.html'))
});

// Start the server
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
  console.log('Press Ctrl+C to quit.');
});