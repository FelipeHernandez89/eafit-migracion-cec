export const environment = {
  apiUrl: 'https://ronellygestiondocumental.table.core.windows.net/',
  apiUrlToken: 'https://ronellygestiondocumental.azurewebsites.net/api/auth/sas/',
  graphApiToken: 'https://ronellygestiondocumental.azurewebsites.net/api/auth/graphApiAD/',
  production: true
};
