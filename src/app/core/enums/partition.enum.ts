export enum Partitions {
    ADDRESS = 'Address',
    USERS = 'Users',
    DIRECTORIES = 'Directories',
    FINAL_DISPOSITION = 'FinalDisposition',
    DOCUMENTARY_TYPES = 'DocumentaryType',
    FILE_STATES = 'FileState',
    CASE_FILES_STATES = 'CasefilesStates',
    ORGANIZATIONAL_TYPES = 'OrganizationalType',
    UNITS_ORGANIZATIONALS = 'OrganizationalUnit',
    STORAGE_TYPES = 'StorageType',
    STORAGE_UNITS = 'StorageUnit',
    SERIES = 'Serie',
    SUB_SERIES = 'Subserie',
    STATES_TRANSACTION = 'StatesTransaction',
    CONFIG_MENU = 'Menu',
    CONFIG_SETTLED = 'ConfigSettled',
    FILEVALUES = 'FileValue',
    DOCUMENTARYTYPES = 'DocumentaryType',
    FINALDISPOSITION = 'FinalDisposition',
    WORK_FLOWS = 'Workflow',
    SETTLEDS = 'Settled',
    AGENDA = 'Agenda',
    DOCUMENT = 'Document'


}