export enum Tables {
    AGENDA="Agenda",
    CONFIGURATIONS = "ListConfiguration",
    CASEFILES = "CaseFile",
    DOCUMENT = "Document",
    USERS = "users",
    CONFIDENTIAL_DOCUMENTS= 'Confidentials',
    ORGANIZATIONAL_TYPES = 'api/organizationalTypes',
}