import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { ROOT_REDUCERS } from './store/reducers';

// Import Services
import { AuthService } from '@services/auth/auth.service';
//Configurations
import {
    ConfigurationService,
    ConfigurationFacadeService,
    
} from '@services/configuration/index';

// import effects
import { AuthEffects } from '@effects/auth.effects';
/* import { DispositionEffects } from '@effects/configurations/disposition.effects'; */
import { ADEffects } from '@effects/ad.effects';
import { AdAuthorizationInterceptor } from '@root/core/interceptors/ad-authorization.interceptor';
import { ConfiUriInterceptor } from './interceptors/config-url.interceptor';
import { AdService } from '@services/active-directory/ad.service';
import { AdFacadeService } from '@services/active-directory/ad-facade.service';
import { TableStorageService } from '@services/table-storage.service';
import { CasefileEffects } from '@effects/management/casefile.effects';
import { configurationsEffects } from '@effects/configurations';
import { ViewsFacadeService } from '@services/management/views-facade.service';
import { managementEffects } from '@effects/management';
import { ConfidentialDocumentFacadeService } from '@services/management';

@NgModule({
    imports: [
        StoreModule.forRoot(ROOT_REDUCERS, {
            runtimeChecks: {
                strictStateImmutability: true,
                strictActionImmutability: true,
                strictStateSerializability: true,
                strictActionSerializability: true,
            },
        }),
        EffectsModule.forRoot([
            ...managementEffects,
            ...configurationsEffects,
            AuthEffects,
            ADEffects,
            CasefileEffects
        ])
    ],
})
export class CoreModule {
    static forRoot() {
        return {
            ngModule: CoreModule,
            providers: [
                { provide: 'AuthServiceInterface', useClass: AuthService },
                { provide: 'ConfigurationService', useClass: ConfigurationService },
                { provide: 'TableStorageService', useClass: TableStorageService },
                { provide: 'AdService', useClass: AdService },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: ConfiUriInterceptor,
                    multi: true
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: AdAuthorizationInterceptor,
                    multi: true
                },
                ConfigurationFacadeService,
                AdFacadeService,
                ViewsFacadeService
            ],
        };
    }
}