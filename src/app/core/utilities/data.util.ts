import { UserModel } from "../models/user.model";
import { MenuItemModel } from '@models/menu-item.model';

export const emptyUser = (): UserModel => {
    return {
        fullName: 'Carolina Ortega',
        email: '',
        firstName: '',
        lastName: '',
        uid: '',
        photoURL: '',
    };
};
export const navBarOptions = (): MenuItemModel[] => {
    return [
        {
            route: ['auth', 'login'],
            icon: 'person'
        },
        {
            route: ['auth', 'sign-up'],
            icon: 'group_add'
        },
        {
            route: ['home', 'news'],
            icon: 'autorenew'
        }]
}

export const menuItemsData = (): MenuItemModel[] => {
    return [
        {
            name: 'init',
            icon: 'home'
        },
        {
            name: 'Vistas',
            icon: 'visibility',
            children: [
                
                /* {
                    name: 'En archivo histórico',
                    icon: 'visibility',
                    route: ['management', 'historicalCasefile']
                }, */
            ]
        },
        {
            name: 'Componente Principal',
            icon: 'policy',
            route: ['management', 'confidential']
        },
        /* {
            name: 'Expedientes',
            icon: 'folder',
            route: ['management', 'previewCasefile']
        }, */
        {
            name: 'Listas',
            icon: 'format_list_bulleted',
            children: [
                /* {
                    name: 'Series',
                    icon: 'person',
                    route: ['configuration', 'series']
                }, */
            ]
        },
        {
            name: 'Configuración',
            icon: 'settings',
            children: [
                
                /* {
                    name: 'Radicados',
                    icon: 'person',
                    route: ['configuration', 'settled']
                } */
            ]
        },
        {

            name: 'Seguridad',
            icon: 'lock',
            children: [
                /* {
                    name: 'Radicados',
                    icon: 'person',
                    route: ['configuration', 'settled']
                } */
            ]
        }
    ]
};