import { Action, createReducer, on } from '@ngrx/store';
import { ViewsActions } from '@actions/index';

export interface State {
    myConfidentials: any[];
}

export const initialState: State = {
    myConfidentials: [],
};


const adReducer = createReducer(
    initialState,
    on(ViewsActions.getMyConfidentialsAction, (state) => ({ ...state, error: null, loading: true })),
    on(ViewsActions.getMyConfidentialsSuccessAction, (state, { data }) => ({ ...state, loading: false, data })),
);


export function reducer(state: State | undefined, action: Action) {
    return adReducer(state, action);
}

export const getMyConfidentials = (state: State) => state.myConfidentials;