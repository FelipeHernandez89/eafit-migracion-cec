import {
    createSelector,
    createFeatureSelector,
    Action,
    ActionReducerMap,
} from '@ngrx/store';
import { InjectionToken } from '@angular/core';

/**
 * Every reducer module's default export is the reducer function itself. In
 * addition, each module should export a type or interface that describes
 * the state of the reducer plus any selector functions. The `* as`
 * notation packages up all of the exports into a single object.
 */
import * as fromAuth from './auth.reducer';
import * as fromConfiguration from './configuration.reducer';
import * as fromManagement from './management.reducer';
import * as fromViews from './views.reducer';

import * as fromAD from './ad.reducer';

/**
 * As mentioned, we treat each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */
export interface State {
    auth: fromAuth.State;
    configuration: fromConfiguration.State;
    ad: fromAD.State;
    management: fromManagement.State,
    views: fromViews.State
}

export const ROOT_REDUCERS = new InjectionToken<ActionReducerMap<State, Action>>
    ('Root reducers token', {
        factory: () => ({
            auth: fromAuth.reducer,
            configuration: fromConfiguration.reducer,
            ad: fromAD.reducer,
            management: fromManagement.reducer,
            views: fromViews.reducer
        }),
    });

/**
* Auth Reducers
*/
export const getAuthState = createFeatureSelector<State, fromAuth.State>(
    'auth'
);

export const getLoggedIn = createSelector(
    getAuthState,
    fromAuth.getLoggedIn
);

export const getUser = createSelector(
    getAuthState,
    fromAuth.getUser
);

export const getLoginPageError = createSelector(
    getAuthState,
    fromAuth.getError
);
export const getAuthLoading = createSelector(
    getAuthState,
    fromAuth.getAuthLoading
);


/**
* Configuration Reducers
*/
export const getConfigutarionState = createFeatureSelector<State, fromConfiguration.State>(
    'configuration'
);

export const getErrorConfiguration = createSelector(
    getConfigutarionState,
    fromConfiguration.getError
);
export const isLoadingConfiguration = createSelector(
    getConfigutarionState,
    fromConfiguration.getLoading
);

export const getMessageConfiguration = createSelector(
    getConfigutarionState,
    fromConfiguration.getMessageAction
);


// Series

export const getSeries = createSelector(
    getConfigutarionState,
    fromConfiguration.getSeries
);
export const getCurrentSeries = createSelector(
    getConfigutarionState,
    fromConfiguration.getCurrentSeries
);


/**
* active directory reducers
*/
export const getADState = createFeatureSelector<State, fromAD.State>(
    'ad'
);


export const getADUsers = createSelector(
    getADState,
    fromAD.getADUsers
);


/**
* Management Reducers
*/
export const getManagementState = createFeatureSelector<State, fromManagement.State>(
    'management'
);

export const getErrorManagement = createSelector(
    getManagementState,
    fromManagement.getError
);
export const isLoadingManagement = createSelector(
    getManagementState,
    fromManagement.getLoading
);

export const getMessageManagement = createSelector(
    getManagementState,
    fromManagement.getMessageAction
);

export const getCasefiles = createSelector(
    getManagementState,
    fromManagement.getCaseFile
);
export const getCurrentCasefile = createSelector(
    getManagementState,
    fromManagement.getCurrentCasefile
);

export const getDocuments = createSelector(
    getManagementState,
    fromManagement.getDocuments
);

export const getCurrentDocuments = createSelector(
    getManagementState,
    fromManagement.getCurrentDocuments
);

export const getRecentlyCreatedDocument= createSelector(
    getManagementState,
    fromManagement.getRecentlyCreatedDocument
);

export const getConfidentialDocs = createSelector(
    getManagementState,
    fromManagement.getConfidentialDocs
);
export const getRecentlyCreatedConfidentialDoc= createSelector(
    getManagementState,
    fromManagement.getRecentlyCreatedConfidentialDoc
);


// Views reducers-----------------------------------------------

export const getViewsState = createFeatureSelector<State, fromViews.State>(
    'views'
);

export const getMyConfidentials = createSelector(
    getViewsState,
    fromViews.getMyConfidentials
);
