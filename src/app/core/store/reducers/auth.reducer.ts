import { Action, createReducer, on } from '@ngrx/store';

import * as AuthActions from '@actions/auth.actions';
import { UserModel, UserGraphApiModel } from '@models/user.model';
import { emptyUser } from '@utilities/data.util';

export interface State {
    loggedIn: boolean;
    user: UserModel | null;
    error: string | null;
    loading: boolean;
}

export const initialState: State = {
    loggedIn: false,
    user: emptyUser(),
    error: null,
    loading: false,
};


const authReducer = createReducer(
    initialState,
    on(AuthActions.login, (state, { credential }) => ({ ...state, error: null, loading: true })),
    on(AuthActions.createUser, (state, { user }) => ({ ...state, error: null, loading: true, user })),
    on(AuthActions.loginSuccess, (state, { user }) => ({
        ...state,
        error: null,
        loading: false,
        user,
        loggedIn: true,
    })),
    on(AuthActions.logoutSuccess, (state) => ({ ...state, ...initialState })),
    on(AuthActions.loginFailure, (state, { error }) => ({ ...state, error, loading: false })),
);

export function reducer(state: State | undefined, action: Action) {
    return authReducer(state, action);
}

export const getLoggedIn = (state: State) => state.loggedIn;
export const getUser = (state: State) => state.user;
export const getADUserS = (state: State) => state.user;
export const getError = (state: State) => state.error;
export const getAuthLoading = (state: State) => state.loading;