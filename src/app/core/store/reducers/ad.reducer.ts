import { Action, createReducer, on } from '@ngrx/store';

import * as AuthActions from '@actions/auth.actions';
import { UserModel, UserGraphApiModel } from '@models/user.model';
import { emptyUser } from '@utilities/data.util';
import { AdActions } from '@actions/index';

export interface State {
    adUsers: UserGraphApiModel[];
}

export const initialState: State = {
    adUsers: [],
};


const adReducer = createReducer(
    initialState,
    on(AdActions.searchADUsers, (state) => ({ ...state, error: null, loading: true })),
    on(AdActions.searchADUsersSuccess, (state, { adUsers }) => ({ ...state, loading: false, adUsers })),
);


export function reducer(state: State | undefined, action: Action) {
    return adReducer(state, action);
}

export const getADUsers = (state: State) => state.adUsers;