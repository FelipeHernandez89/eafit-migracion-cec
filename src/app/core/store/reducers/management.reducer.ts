import { Action, createReducer, on } from '@ngrx/store';

import * as CasefileActions from '@actions/management/casefile.action';
import * as DocumentActions from '@actions/management/document.action';
import * as ConfidentialDocActions from '@actions/management/confidentialDocs.action';

import * as ConfigurationActions from '@actions/configuration/configuration.action';
import { CasefileModel } from '@models/management/casefile.model';
import { DocumentModel } from '@models/management/document.model';
import { ConfidentialDocModel } from '@models/management/confidentialDoc.model';

export interface State {
    casefiles: CasefileModel[];
    currentCasefile: CasefileModel;
    documents: DocumentModel[];
    currentDocuments: DocumentModel;
    confidentialDocs: ConfidentialDocModel[];
    recentlyCreatedconfidentialDoc: ConfidentialDocModel;
    recentlyCreatedDocument: DocumentModel;
    error: string | null;
    loading: boolean;
    message: string | null
}

export const initialState: State = {
    casefiles: [],
    currentCasefile: null,
    documents: [],
    currentDocuments: null,
    recentlyCreatedDocument: null,
    confidentialDocs: [],
    recentlyCreatedconfidentialDoc: null,
    error: null,
    loading: true,
    message: null
};


const configurationReducer = createReducer(
    initialState,
    //**************************** CaseFiles ************************************* */
    on(CasefileActions.searchCasefiles, (state) => ({ ...state, error: null, loading: true })),
    on(CasefileActions.searchCasefilesSuccess, (state, { casefiles }) => ({ ...state, loading: false, casefiles })),
    on(CasefileActions.historicalCaseFile, (state) => ({ ...state, error: null, loading: true })),
    on(CasefileActions.historicalCaseFileSuccess, (state, { casefiles }) => ({ ...state, loading: false, casefiles })),
    on(CasefileActions.createCasefile, (state) => updateFlags(state)),
    on(CasefileActions.createCasefileSuccess, (state, { casefile }) => ({
        ...state,
        loading: false,
        message: "Disposicion creada correctamente...",
        Casefile: [...state.casefiles, casefile]
    })),
    on(CasefileActions.updateCasefile, (state) => updateFlags(state)),
    on(CasefileActions.updateCasefileSuccess, (state, { casefile }) => {
        const filter = state
            .casefiles
            .filter(item => item.RowKey !== casefile.RowKey)
        return {
            ...state,
            loading: false,
            message: "Disposicion actualizada correctamente...",
            dispositions: [...filter, casefile],
            currentCasefile: null,
        }
    }),
    on(CasefileActions.deleteCasefile, (state) => deleteFlags(state)),
    on(CasefileActions.deleteCasefileSuccess, (state, { Casefile }) => {
        const filter = state
            .casefiles
            .filter(item => item.RowKey !== Casefile.RowKey)
        return {
            ...state,
            loading: false,
            message: "Tipo de organización eliminado correctamente...",
            Casefiles: [...filter, Casefile],
            currentCasefile: null,
        }
    }),
    on(CasefileActions.selectCasefile, (state, { casefile }) => ({
        ...state,
        currentCasefile: casefile
    })),


    //**************************** Document ************************************* */
    on(DocumentActions.searchDocuments, (state) => ({ ...state, error: null, loading: true })),
    on(DocumentActions.searchDocumentsSuccess, (state, { Documents }) => ({ ...state, loading: false, documents: Documents })),
    on(DocumentActions.createDocument, (state) => updateFlags(state)),
    on(DocumentActions.createDocumentSuccess, (state, { Document }) => ({
        ...state,
        loading: false,
        message: "Documento creado correctamente...",
        documents: [...state.documents, Document],
        recentlyCreatedDocument: Document
    })),
    on(DocumentActions.updateDocument, (state) => updateFlags(state)),
    on(DocumentActions.updateDocumentSuccess, (state, { Document }) => {
        const filter = state
            .documents
            .filter(item => item.RowKey !== Document.RowKey)
        return {
            ...state,
            loading: false,
            message: "Documento actualizado correctamente...",
            dispositions: [...filter, Document],
        }
    }),
    on(DocumentActions.deleteDocument, (state) => deleteFlags(state)),
    on(DocumentActions.deleteDocumentSuccess, (state, { Document }) => {
        const filter = state
            .documents
            .filter(item => item.RowKey !== Document.RowKey)
        return {
            ...state,
            loading: false,
            message: "Documento eliminado correctamente...",
            documents: [...filter, Document],
        }
    }),
    on(ConfigurationActions.configurationError, (state, { error }) => ({ ...state, error, loading: false })),

    //********************************************************************************************************** */
    //**************************** confidential Documents ************************************* */
    on(ConfidentialDocActions.searchConfidentialDocs, (state) => ({ ...state, error: null, loading: true })),
    on(ConfidentialDocActions.searchConfidentialDocsSuccess, (state, { ConfidentialDocs }) => ({ ...state, loading: false, confidentialDocs: ConfidentialDocs })),
    on(ConfidentialDocActions.createConfidentialDoc, (state) => updateFlags(state)),
    on(ConfidentialDocActions.createConfidentialDocSuccess, (state, { ConfidentialDoc }) => ({
        ...state,
        loading: false,
        message: "Documento confidencial creado correctamente...",
        confidentialDocs: [...state.confidentialDocs, ConfidentialDoc],
        recentlyCreatedconfidentialDoc: ConfidentialDoc
    })),
    on(ConfidentialDocActions.updateConfidentialDoc, (state: any) => updateFlags(state)),
    on(ConfidentialDocActions.updateConfidentialDocSuccess, (state, { ConfidentialDoc }) => {
        const filter = state
            .confidentialDocs
            .filter(item => item.RowKey !== ConfidentialDoc.RowKey)
        return {
            ...state,
            loading: false,
            message: "Documento confidencial actualizado correctamente...",
            dispositions: [...filter, Document],
        }
    }),
    on(ConfidentialDocActions.deleteConfidentialDoc, (state) => deleteFlags(state)),
    on(ConfidentialDocActions.deleteConfidentialDocSuccess, (state, { ConfidentialDoc }) => {
        const filter = state
            .confidentialDocs
            .filter(item => item.RowKey !== ConfidentialDoc.RowKey)
        return {
            ...state,
            loading: false,
            message: "Documento confidencial eliminado correctamente...",
            documents: [...filter, Document],
        }
    }),
);

//********************************************************************************************************** */
const updateFlags = (state: State): State => {
    return {
        ...state,
        error: null,
        loading: true,
        message: null
    }
}
const deleteFlags = (state: State): State => {
    return {
        ...state,
        error: null,
        loading: true,
        message: null
    }
}

export function reducer(state: State | undefined, action: Action) {
    return configurationReducer(state, action);
}

export const getDocuments = (state: State) => state.documents;
export const getCurrentDocuments = (state: State) => state.currentDocuments;

export const getCaseFile = (state: State) => state.casefiles;
export const getCurrentCasefile = (state: State) => state.currentCasefile;

export const getError = (state: State) => state.error;
export const getLoading = (state: State) => state.loading;
export const getMessageAction = (state: State) => state.message;
export const getRecentlyCreatedDocument = (state: State) => state.recentlyCreatedDocument;

export const getRecentlyCreatedConfidentialDoc = (state: State) => state.recentlyCreatedconfidentialDoc;
export const getConfidentialDocs = (state: State) => state.confidentialDocs;