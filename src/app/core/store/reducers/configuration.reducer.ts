import { Action, createReducer, on } from '@ngrx/store';
import * as ConfigurationActions from '@actions/configuration/configuration.action';
import { OrganizationalTypesModel } from '@models/configuration/organizationalType.model';
import { UnitOrganizationalModel } from '@models/configuration/unitOrganizational.model';
import { StorageTypesModel } from '@models/configuration/storageType.model';
import { StorageUnitsModel } from '@models/configuration/StorageUnit.model';
import { SeriesModel } from '@models/configuration/Series.model';
import { SubSeriesModel } from '@models/configuration/SubSeries.model';
import { StatesTransactionModel } from '@models/configuration/StatesTransaction.model';
import { ConfigMenuModel } from '@models/configuration/ConfigMenu.model';
import { ConfigSettledModel } from '@models/configuration/ConfigSettled.model';
import { SeriesAction,} from '@actions/index';
import { } from '@actions/index';
import { FileValuesModel } from '@models/configuration/file-value.model';
import { DocumentaryTypesModel } from '@models/configuration/documentaryType.model';
import { DispositionFinalsModel } from '@models/configuration/dispositionFinal.model';
import { FileStatesModel } from '@models/configuration/fileState.models';
import { WorkFlowsModel } from '@models/configuration/workFlow.model';
import {SettledsModel} from '@models/configuration/settled.model';
import {AgendaModel} from '@models/configuration/agenda.model';
import { DocumentModel } from '@models/management/document.model';
export interface State {
    organizationalTypes: OrganizationalTypesModel[]
    currentOrganizationalType: OrganizationalTypesModel
    dispositionFinal: DispositionFinalsModel[]
    currentDispositionFinal: DispositionFinalsModel
    workFlow: WorkFlowsModel[]
    currentWorkFlow: WorkFlowsModel 
    fileState: FileStatesModel[]
    currentFileState: FileStatesModel 
    settled: SettledsModel[]
    currentSettled: SettledsModel
    fileValues:FileValuesModel[];
    currentFileValue: FileValuesModel
    documentaryType: DocumentaryTypesModel[]
    currentDocumentaryType: DocumentaryTypesModel
    unitsOrganizationals: UnitOrganizationalModel[]
    currentUnitOrganizational: UnitOrganizationalModel;
    StorageTypes: StorageTypesModel[]
    currentStorageType: StorageTypesModel
    StorageUnits: StorageUnitsModel[]
    currentStorageUnit: StorageUnitsModel
    SubSeries: SubSeriesModel[]
    currentSubSeries: SubSeriesModel
    Series: SeriesModel[]
    currentSeries: SeriesModel
    Agenda: AgendaModel[]
    currentAgenda: AgendaModel
    StatesTransaction: StatesTransactionModel[]
    currentStatesTransaction: StatesTransactionModel
    ConfigMenu: ConfigMenuModel[]
    currentConfigMenu: ConfigMenuModel
    ConfigSettled: ConfigSettledModel[]
    currentConfigSettled: ConfigSettledModel
    error: string | null;
    loading: boolean;
    message: string | null
}

export const initialState: State = {
    unitsOrganizationals: [],
    currentUnitOrganizational: null,
    organizationalTypes: [],
    currentOrganizationalType: null,
    StorageTypes : null,
    currentStorageType: null,
    StorageUnits: null,
    currentStorageUnit: null,
    SubSeries: null,
    currentSubSeries: null,
    Series: null,
    currentSeries: null,
    Agenda: null,
    currentAgenda: null,
    StatesTransaction: null,
    currentStatesTransaction: null,
    ConfigMenu: null,
    currentConfigMenu: null,
    ConfigSettled: null,
    currentConfigSettled: null,
    documentaryType: [],
    currentDocumentaryType: null,
    dispositionFinal: [],
    currentDispositionFinal: null,
    workFlow:  [],
    currentWorkFlow: null,
    fileState: [],
    currentFileState: null,
    settled: [],
    currentSettled: null,
    fileValues:[],
    currentFileValue: null,
    error: null,
    loading: false,
    message: null
};


const configurationReducer = createReducer(
    initialState,
    //---------------------------------------------------------------------

    //---------------------------Series-----------------------------------------
    //---------------------------------------------------------------------
    on(SeriesAction.searchSeries, (state) => ({ ...state, error: null, loading: true })),
    on(SeriesAction.searchSeriesSuccess, (state, { Series }) => ({ ...state, loading: false, Series })),
    on(SeriesAction.createSeries, (state) => updateFlags(state)),
    on(SeriesAction.createSeriesSuccess, (state, { Series }) => ({
        ...state,
        loading: false,
        message: "Tipo de organización fue creada correctamente...",
        Series: [...state.Series, Series]
    })),
    on(SeriesAction.updateSeries, (state) => updateFlags(state)),
    on(SeriesAction.updateSeriesSuccess, (state, { Series }) => {
        const filter = state
            .Series
            .filter(item => item.RowKey !== Series.RowKey)
        return {
            ...state,
            loading: false,
            message: "Tipo de organización actualizada correctamente...",
            Series: [...filter, Series],
            currentSeries: null,
        }
    }),
    on(SeriesAction.deleteSeries, (state) => deleteFlags(state)),
    on(SeriesAction.deleteSeriesSuccess, (state, { Series }) => {
        const filter = state
            .Series
            .filter(item => item.RowKey !== Series.RowKey)
        return {
            ...state,
            loading: false,
            message: "Tipo de organización eliminado correctamente...",
            Series: [...filter, Series],
            currentSeries: null,
        }
    }),
    on(SeriesAction.selectSeries, (state, { Series }) => ({
        ...state,
        currentSeries: Series
    })),

    
       //---------------------------------------------------------------------
    on(ConfigurationActions.configurationError, (state, { error }) => ({ ...state, error, loading: false })),
);

const updateFlags = (state: State): State => {
    return {
        ...state,
        error: null,
        loading: true,
        message: null
    }
}
const deleteFlags = (state: State): State => {
    return {
        ...state,
        error: null,
        loading: true,
        message: null
    }
}
export function reducer(state: State | undefined, action: Action) {
    return configurationReducer(state, action);
}


export const getSeries= (state: State) => state.Series;
export const getCurrentSeries = (state: State) => state.currentSeries;

export const getError = (state: State) => state.error;
export const getLoading = (state: State) => state.loading;
export const getMessageAction = (state: State) => state.message;