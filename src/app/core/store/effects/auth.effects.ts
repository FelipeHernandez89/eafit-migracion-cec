import { Injectable, Inject } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { Router } from '@angular/router';

import { AuthServiceInterface } from '@interfaces/auth-service.interface';
import {
    AuthActions
} from '@actions/index';
import { UserModel } from '@models/user.model';


@Injectable()
export class AuthEffects {

    login$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.login),
            switchMap(({ credential }) =>
                this.authService
                    .login$(credential)
                    .pipe(
                        map((user: UserModel) => AuthActions.loginSuccess({ user })),
                        catchError(error => of(AuthActions.loginFailure({ error })))
                    )
            ))
    );

    createUser$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.createUser),
            switchMap(({ user }) =>
                this.authService
                    .createUser$(user)
                    .pipe(
                        map((user: UserModel) => AuthActions.loginSuccess({ user })),
                        catchError(error => of(AuthActions.loginFailure({ error })))
                    )
            ))
    );

    loginSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.loginSuccess),
            tap(() => this.router.navigate(['home']))
        ),
        { dispatch: false }
    );

    logout$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.logout),
            switchMap(() =>
                this.authService
                    .logout$()
                    .pipe(
                        map(() => AuthActions.logoutSuccess()),
                        catchError(error => of(AuthActions.loginFailure({ error })))
                    )
            ))
    );

    constructor(
        private actions$: Actions,
        @Inject('AuthServiceInterface') private authService: AuthServiceInterface,
        private router: Router
    ) { }
}