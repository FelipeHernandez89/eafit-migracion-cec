import { Injectable, Inject } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import {
    ConfidentialDocsActions, ConfigurationActions
} from '@actions/index';
import { TableStorageService } from '@services/table-storage.service';
import { Router } from '@angular/router';
import { ConfidentialDocModel } from '@models/management/confidentialDoc.model';


@Injectable()
export class ConfidentialDocEffects {
    // Documents Effects
    searchDocuments$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ConfidentialDocsActions.searchConfidentialDocs),
            switchMap(({ action }) =>
                this.tableStorageService
                    .search$(action)
                    .pipe(
                        map((ConfidentialDocs: ConfidentialDocModel[]) => ConfidentialDocsActions
                            .searchConfidentialDocsSuccess({ ConfidentialDocs })
                        ),
                        catchError(error => of(ConfigurationActions.configurationError({ error })))
                    )
            ))
    );

    createDocument$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ConfidentialDocsActions.createConfidentialDoc),
            switchMap(({ action }) =>
                this.tableStorageService
                    .create$(action)
                    .pipe(
                        map((ConfidentialDoc: ConfidentialDocModel) => ConfidentialDocsActions.createConfidentialDocSuccess({ ConfidentialDoc })
                        ),
                        catchError(error => of(ConfigurationActions.configurationError({ error })))
                    )
            ))
    );




    updateDocument$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ConfidentialDocsActions.updateConfidentialDoc),
            switchMap(({ action }) =>
                this.tableStorageService
                    .update$(action)
                    .pipe(
                        map((ConfidentialDoc: ConfidentialDocModel) => ConfidentialDocsActions
                            .updateConfidentialDocSuccess({ ConfidentialDoc })
                        ),
                        catchError(error => of(ConfigurationActions.configurationError({ error })))
                    )
            ))
    );

    deleteDocument$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ConfidentialDocsActions.deleteConfidentialDoc),
            switchMap(({ action }) =>
                this.tableStorageService
                    .delete$(action)
                    .pipe(
                        map((ConfidentialDoc: ConfidentialDocModel) => ConfidentialDocsActions
                            .deleteConfidentialDocSuccess({ ConfidentialDoc })
                        ),
                        catchError(error => of(ConfigurationActions.configurationError({ error })))
                    )
            ))
    );
    /////////////
    constructor(
        private actions$: Actions,
        @Inject('TableStorageService') private tableStorageService: TableStorageService,
    ) { }
}