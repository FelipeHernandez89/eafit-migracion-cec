import { CasefileEffects } from './casefile.effects';
import { DocumentEffects } from './document.effects';
import { ConfidentialDocEffects } from './confidentialDoc';


export const managementEffects = [
    CasefileEffects,
    DocumentEffects,
    ConfidentialDocEffects

]