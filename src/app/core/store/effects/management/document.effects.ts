import { Injectable, Inject } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import {
    ConfigurationActions,
    DocumentActions
} from '@actions/index';
import { TableStorageService } from '@services/table-storage.service';
import { Router } from '@angular/router';
import { DocumentModel } from '@models/management/document.model';


@Injectable()
export class DocumentEffects {
    // Documents Effects
    searchDocuments$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DocumentActions.searchDocuments),
            switchMap(({ action }) =>
                this.tableStorageService
                    .search$(action)
                    .pipe(
                        map((Documents: DocumentModel[]) => DocumentActions
                            .searchDocumentsSuccess({ Documents })
                        ),
                        catchError(error => of(ConfigurationActions.configurationError({ error })))
                    )
            ))
    );

    createDocument$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DocumentActions.createDocument),
            switchMap(({ action }) =>
                this.tableStorageService
                    .create$(action)
                    .pipe(
                        map((Document: DocumentModel) => DocumentActions.createDocumentSuccess({ Document })
                        ),
                        catchError(error => of(ConfigurationActions.configurationError({ error })))
                    )
            ))
    );




    updateDocument$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DocumentActions.updateDocument),
            switchMap(({ action }) =>
                this.tableStorageService
                    .update$(action)
                    .pipe(
                        map((Document: DocumentModel) => DocumentActions
                            .updateDocumentSuccess({ Document })
                        ),
                        catchError(error => of(ConfigurationActions.configurationError({ error })))
                    )
            ))
    );

    deleteDocument$ = createEffect(() =>
        this.actions$.pipe(
            ofType(DocumentActions.deleteDocument),
            switchMap(({ action }) =>
                this.tableStorageService
                    .delete$(action)
                    .pipe(
                        map((Document: DocumentModel) => DocumentActions
                            .deleteDocumentSuccess({ Document })
                        ),
                        catchError(error => of(ConfigurationActions.configurationError({ error })))
                    )
            ))
    );
    /////////////
    constructor(
        private actions$: Actions,
        @Inject('TableStorageService') private tableStorageService: TableStorageService,
    ) { }
}