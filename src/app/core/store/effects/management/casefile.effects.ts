import { Injectable, Inject } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import {
    ConfigurationActions,
    CasefileActions
} from '@actions/index';
import { TableStorageService } from '@services/table-storage.service';
import { CasefileModel } from '@models/management/casefile.model';
import { Router } from '@angular/router';


@Injectable()
export class CasefileEffects {
    // casefiles Effects
    searchCasefiles$ = createEffect(() =>
        this.actions$.pipe(
            ofType(CasefileActions.searchCasefiles),
            switchMap(({ action }) =>
                this.tableStorageService
                    .search$(action)
                    .pipe(
                        map((casefiles: CasefileModel[]) => CasefileActions
                            .searchCasefilesSuccess({ casefiles })
                        ),
                        catchError(error => of(ConfigurationActions.configurationError({ error })))
                    )
            ))
    );

    createCasefile$ = createEffect(() =>
        this.actions$.pipe(
            ofType(CasefileActions.createCasefile),
            switchMap(({ action }) =>
                this.tableStorageService
                    .create$(action)
                    .pipe(
                        map((casefile: CasefileModel) => CasefileActions
                        .createCasefileSuccess({ casefile })
                        ),
                        tap(s =>this.router.navigate(['management/previewCasefile'])),
                        catchError(error => of(ConfigurationActions.configurationError({ error })))
                    )
            ))
    );

    updateCasefile$ = createEffect(() =>
        this.actions$.pipe(
            ofType(CasefileActions.updateCasefile),
            switchMap(({ action }) =>
                this.tableStorageService
                    .update$(action)
                    .pipe(
                        map((casefile: CasefileModel) => CasefileActions
                            .updateCasefileSuccess({ casefile })
                        ),
                        tap(s =>this.router.navigate(['management/previewCasefile'])),
                        catchError(error => of(ConfigurationActions.configurationError({ error })))
                    )
            ))
    );

    deleteCasefile$ = createEffect(() =>
        this.actions$.pipe(
            ofType(CasefileActions.deleteCasefile),
            switchMap(({ action }) =>
                this.tableStorageService
                    .delete$(action)
                    .pipe(
                        map((Casefile: CasefileModel) => CasefileActions
                            .deleteCasefileSuccess({ Casefile })
                        ),
                        catchError(error => of(ConfigurationActions.configurationError({ error })))
                    )
            ))
    );
    /////////////
    constructor(
        private actions$: Actions,
        @Inject('TableStorageService') private tableStorageService: TableStorageService,
        private router: Router
    ) { }
}