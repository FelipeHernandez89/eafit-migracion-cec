import { Injectable, Inject } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import {
    ConfigurationActions,
    ViewsActions
} from '@actions/index';
import { TableStorageService } from '@services/table-storage.service';


@Injectable()
export class ViewsEffects {
    // casefiles Effects
    getMyConfidentials$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ViewsActions.getMyConfidentialsAction),
            switchMap(({ action }) =>
                this.tableStorageService
                    .search$(action)
                    .pipe(
                        map((data: any) => ViewsActions
                            .getMyConfidentialsSuccessAction({ data })
                        ),
                        catchError(error => of(ConfigurationActions.configurationError({ error })))
                    )
            ))
    );

    /////////////
    constructor(
        private actions$: Actions,
        @Inject('TableStorageService') private tableStorageService: TableStorageService,
    ) { }
}