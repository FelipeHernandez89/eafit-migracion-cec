import { Injectable, Inject } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import {
    ConfigurationActions,
    SeriesAction
} from '@actions/index';
import { ConfigurationServiceInterface } from '@interfaces/configuration-service.interface';
import { TableStorageService } from '@services/table-storage.service';
import { SeriesModel } from '@models/configuration/series.model';


@Injectable()
export class SeriesEffects {
    // Series Effects
    searchSeries$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SeriesAction.searchSeries),
            switchMap(({ action }) =>
                this.tableStorageService
                    .search$(action)
                    .pipe(
                        map((Series: SeriesModel[]) => SeriesAction
                            .searchSeriesSuccess({ Series })
                        ),
                        catchError(error => of(ConfigurationActions.configurationError({ error })))
                    )
            ))
    );

    createSeries$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SeriesAction.createSeries),
            switchMap(({ action }) =>
                this.tableStorageService
                    .create$(action)
                    .pipe(
                        map((Series: SeriesModel) => SeriesAction
                            .createSeriesSuccess({ Series })
                        ),
                        catchError(error => of(ConfigurationActions.configurationError({ error })))
                    )
            ))
    );

    updateSeries$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SeriesAction.updateSeries),
            switchMap(({ action }) =>
                this.tableStorageService
                    .update$(action)
                    .pipe(
                        map((Series: SeriesModel) => SeriesAction
                            .updateSeriesSuccess({ Series })
                        ),
                        catchError(error => of(ConfigurationActions.configurationError({ error })))
                    )
            ))
    );

    deleteSeries$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SeriesAction.deleteSeries),
            switchMap(({ action }) =>
                this.tableStorageService
                    .delete$(action)
                    .pipe(
                        map((Series: SeriesModel) => SeriesAction
                            .deleteSeriesSuccess({ Series })
                        ),
                        catchError(error => of(ConfigurationActions.configurationError({ error })))
                    )
            ))
    );
    /////////////
    constructor(
        private actions$: Actions,
        @Inject('TableStorageService') private tableStorageService: TableStorageService,
    ) { }
}