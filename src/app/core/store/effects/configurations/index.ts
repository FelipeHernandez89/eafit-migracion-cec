
import { SeriesEffects } from './series.effects';
import { ViewsEffects } from '@effects/views/views.effect';



export const configurationsEffects = [
    ViewsEffects,
    SeriesEffects,
]