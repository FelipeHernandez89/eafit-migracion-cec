import { Injectable, Inject } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import {
    AdActions, ConfigurationActions
} from '@actions/index';
import { UserGraphApiModel } from '@models/user.model';
import { ADServiceInterface } from '@interfaces/ad-service.interface';


@Injectable()
export class ADEffects {

    // ad Effects
    searchUsers$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AdActions.searchADUsers),
            switchMap(({ action }) =>
                this.adService.search$(action)
                    .pipe(
                        map((adUsers: UserGraphApiModel[]) => AdActions
                            .searchADUsersSuccess({ adUsers })
                        ),
                        catchError(error => of(ConfigurationActions.configurationError({ error: `Error ${error}` })))
                    )
            ))
    );

    constructor(
        private actions$: Actions,
        @Inject('AdService') private adService: ADServiceInterface) { }
}