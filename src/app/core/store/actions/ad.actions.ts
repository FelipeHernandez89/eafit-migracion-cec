import { props, createAction } from '@ngrx/store';
import { UserGraphApiModel } from '@models/user.model';
import { ADActionModel } from '@models/ad/ad-action.model';


export const searchADUsers= createAction(
    '[AD] Search AD users',
    props<{ action: ADActionModel }>()
);

export const searchADUsersSuccess = createAction(
    '[AD] Search Ad users success',
    props<{ adUsers: UserGraphApiModel[] }>()
);