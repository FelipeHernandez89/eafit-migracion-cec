import { createAction, props } from '@ngrx/store';

import { TableStorageActionModel } from '@models/common/table-storage-action.model';
import { DocumentModel } from '@models/management/document.model';

export const createDocument = createAction(
    '[Document] Create Document',
    props<{ action: TableStorageActionModel }>()
);

export const createDocumentSuccess = createAction(
    '[Document] Create Document Success',
    props<{ Document: DocumentModel }>()
);

export const searchDocuments = createAction(
    '[Document] Search Document',
    props<{ action: TableStorageActionModel }>()
);

export const searchDocumentsSuccess = createAction(
    '[Document] Search Documents Success',
    props<{ Documents: DocumentModel[] }>()
);

export const updateDocument = createAction(
    '[Document] Update Document',
    props<{ action: TableStorageActionModel }>()
);

export const updateDocumentSuccess = createAction(
    '[Document] Update Document Success',
    props<{ Document: DocumentModel }>()
);

export const selectDocument = createAction(
    '[Document] Select Document',
    props<{ Document: DocumentModel }>()
);

export const deleteDocument = createAction(
    '[Document] Delete Document',
    props<{ action: TableStorageActionModel }>()
);

export const deleteDocumentSuccess = createAction(
    '[Document] Delete Document Success',
    props<{ Document: DocumentModel }>()
)