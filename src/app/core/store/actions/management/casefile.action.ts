import { createAction, props } from '@ngrx/store';

import { TableStorageActionModel } from '@models/common/table-storage-action.model';
import { CasefileModel } from '@models/management/casefile.model';

export const createCasefile = createAction(
    '[Casefile] Create casefile',
    props<{ action: TableStorageActionModel }>()
);

export const createCasefileSuccess = createAction(
    '[Casefile] Create casefile Success',
    props<{ casefile: CasefileModel }>()
);

export const searchCasefiles = createAction(
    '[Casefile] Search Casefiles',
    props<{ action: TableStorageActionModel }>()
);

export const historicalCaseFile = createAction(
    '[Casefile] Search historicalCasefiles',
    props<{ action: TableStorageActionModel }>()
);

export const historicalCaseFileSuccess = createAction(
    '[Casefile] Search historicalCasefiles Success',
    props<{ casefiles: CasefileModel[] }>()
);

export const searchCasefilesSuccess = createAction(
    '[Casefile] Search casefiles Success',
    props<{ casefiles: CasefileModel[] }>()
);

export const updateCasefile = createAction(
    '[Casefile] Update casefile',
    props<{ action: TableStorageActionModel }>()
);

export const updateCasefileSuccess = createAction(
    '[Casefile] Update casefile Success',
    props<{ casefile: CasefileModel }>()
);

export const selectCasefile = createAction(
    '[Casefile] Select casefile',
    props<{ casefile: CasefileModel }>()
);

export const deleteCasefile = createAction(
    '[Casefile] Delete Casefile',
    props<{ action: TableStorageActionModel }>()
);

export const deleteCasefileSuccess = createAction(
    '[Casefile] Delete Casefile Success',
    props<{ Casefile: CasefileModel }>()
)