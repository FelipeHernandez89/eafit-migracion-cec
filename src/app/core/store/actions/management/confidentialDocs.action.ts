import { createAction, props } from '@ngrx/store';

import { TableStorageActionModel } from '@models/common/table-storage-action.model';
import { ConfidentialDocModel } from '@models/management/confidentialDoc.model';

export const createConfidentialDoc = createAction(
    '[ConfidentialDoc] Create ConfidentialDoc',
    props<{ action: TableStorageActionModel }>()
);

export const createConfidentialDocSuccess = createAction(
    '[ConfidentialDoc] Create ConfidentialDoc Success',
    props<{ ConfidentialDoc: ConfidentialDocModel }>()
);

export const searchConfidentialDocs = createAction(
    '[ConfidentialDoc] Search ConfidentialDoc',
    props<{ action: TableStorageActionModel }>()
);

export const searchConfidentialDocsSuccess = createAction(
    '[ConfidentialDoc] Search ConfidentialDocs Success',
    props<{ ConfidentialDocs: ConfidentialDocModel[] }>()
);

export const updateConfidentialDoc = createAction(
    '[ConfidentialDoc] Update ConfidentialDoc',
    props<{ action: TableStorageActionModel }>()
);

export const updateConfidentialDocSuccess = createAction(
    '[ConfidentialDoc] Update ConfidentialDoc Success',
    props<{ ConfidentialDoc: ConfidentialDocModel }>()
);

export const selectConfidentialDoc = createAction(
    '[ConfidentialDoc] Select ConfidentialDoc',
    props<{ ConfidentialDoc: ConfidentialDocModel }>()
);

export const deleteConfidentialDoc = createAction(
    '[ConfidentialDoc] Delete ConfidentialDoc',
    props<{ action: TableStorageActionModel }>()
);

export const deleteConfidentialDocSuccess = createAction(
    '[ConfidentialDoc] Delete ConfidentialDoc Success',
    props<{ ConfidentialDoc: ConfidentialDocModel }>()
)