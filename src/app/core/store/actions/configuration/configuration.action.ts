import { createAction, props } from '@ngrx/store';

export const configurationError = createAction(
    '[Configuration] Configuration Error',
    props<{ error: any }>()
);