import { createAction, props } from '@ngrx/store';
import { TableStorageActionModel } from '@models/common/table-storage-action.model';
import { SeriesModel } from '@models/configuration/series.model';

export const createSeries = createAction(
    '[Series] Create Series',
    props<{ action: TableStorageActionModel }>()
);

export const createSeriesSuccess = createAction(
    '[Series] Create Series Success',
    props<{ Series: SeriesModel }>()
);

export const searchSeries = createAction(
    '[Series] Search Series',
    props<{ action: TableStorageActionModel }>()
);

export const searchSeriesSuccess = createAction(
    '[Series] Search Series Success',
    props<{ Series: SeriesModel[] }>()
);

export const updateSeries = createAction(
    '[Series] Update Series',
    props<{ action: TableStorageActionModel }>()
);

export const updateSeriesSuccess = createAction(
    '[Series] Update Series Success',
    props<{ Series: SeriesModel }>()
);

export const selectSeries = createAction(
    '[Series] Select Series',
    props<{ Series: SeriesModel }>()
);

export const deleteSeries = createAction(
    '[Series] Delete Series',
    props<{ action: TableStorageActionModel }>()
);

export const deleteSeriesSuccess = createAction(
    '[Series] Delete Series Success',
    props<{ Series: SeriesModel }>()
);