import { createAction, props } from '@ngrx/store';

import { UserModel } from '@models/user.model';
import { Authenticate } from '@models/Authenticate.model';


export const login = createAction(
    '[Auth] Login',
    props<{ credential: Authenticate }>()
);

export const loginSuccess = createAction(
    '[Auth] Login Success',
    props<{ user: UserModel }>()
);

export const loginFailure = createAction(
    '[Auth] Login Failure',
    props<{ error: any }>()
);

export const logout = createAction(
    '[Auth] Logout',
);

export const logoutSuccess = createAction(
    '[Auth] Logout Success',
);

export const createUser = createAction(
    '[Auth] Create User',
    props<{ user: UserModel }>()
);