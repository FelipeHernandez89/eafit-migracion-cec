import { createAction, props } from '@ngrx/store';
import { TableStorageActionModel } from '@models/common/table-storage-action.model';

export const getMyConfidentialsAction = createAction(
    '[Views] my confidentials get',
    props<{ action: TableStorageActionModel }>()
);


export const getMyConfidentialsSuccessAction = createAction(
    '[Views] my confidentials get success',
    props<{ data: any }>()
);