import * as AuthActions from './auth.actions';
import * as CasefileActions from './management/casefile.action'
import * as DocumentActions from './management/document.action'
import * as SeriesAction from './configuration/series.action';
import * as ConfigurationActions from './configuration/configuration.action';
import * as ViewsActions from './views/views.actions'
import * as ConfidentialDocsActions from './management/confidentialDocs.action'
import * as AdActions from './ad.actions';

export {
    ConfidentialDocsActions,
    ViewsActions,
    AuthActions,
    ConfigurationActions,
    AdActions,
    CasefileActions,
    DocumentActions,
    SeriesAction,
};
