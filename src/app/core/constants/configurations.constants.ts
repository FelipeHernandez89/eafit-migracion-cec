import { of } from 'rxjs';

export const listSexo$ = of([
    {id: 1, name:'Masculino'},
    {id: 2, name: 'Femenino'}
])