import { Injectable, Inject } from '@angular/core';
import { HttpEvent, HttpRequest, HttpHandler, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

import { environment } from '@environments/environment'
import { catchError, mergeMap } from 'rxjs/operators';
import { ConfigurationServiceInterface } from '@interfaces/configuration-service.interface';
import { ADServiceInterface } from '@interfaces/ad-service.interface';
import { TableStorageServiceInterface } from '@interfaces/table-storage.interface';

@Injectable()
export class AdAuthorizationInterceptor implements HttpInterceptor {

    constructor(
        @Inject('AdService') private adService: ADServiceInterface,
        @Inject('TableStorageService') private tableStorageService: TableStorageServiceInterface,
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (request.url == environment.apiUrlToken || request.url == environment.graphApiToken) {
            return next.handle(request);
        }
        if (request.url.indexOf(environment.graphApiUrl) > -1) {
            return this.getTokenConfig(false, next, request)
                .pipe(
                    catchError((error: HttpErrorResponse) => {
                        this.tableStorageService.loggerErrorHttp(error)
                        if (error && error.status === 401) {
                            return this.getTokenConfig(true, next, request)
                        }
                        return throwError(error);
                    })
                )
        }
        return next.handle(request);
    }

    private getTokenConfig(isExpired: boolean, next: HttpHandler, request: HttpRequest<any>): Observable<HttpEvent<any>> {
        return this.adService
            .getToken$(isExpired)
            .pipe(
                mergeMap(token => next.handle(
                    this.addAuthenticationToken(token, request)
                )),
            )
    }
    private addAuthenticationToken(token: string, request: HttpRequest<any>): HttpRequest<any> {
        // TODO VALIDATE URL EXTERN API
        if (request.url == environment.graphApiToken) {
            return request;
        }
        let url = request.url
        return request.clone({
            url,
            setHeaders: {
                ['Authorization']: `bearer ${token}`,
                ['Accept']: 'application/json;odata=nometadata'
            }
        });
    }
}