import { Injectable, Inject } from '@angular/core';
import { HttpEvent, HttpRequest, HttpHandler, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

import { environment } from '@environments/environment'
import { catchError, mergeMap } from 'rxjs/operators';
import { ConfigurationServiceInterface } from '@interfaces/configuration-service.interface';
import { TableStorageServiceInterface } from '@interfaces/table-storage.interface';

@Injectable()
export class ConfiUriInterceptor implements HttpInterceptor {

    constructor(
        @Inject('TableStorageService') private tableStorageService: TableStorageServiceInterface,
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (request.url == environment.apiUrlToken || request.url == environment.graphApiToken) {
            return next.handle(request);
        }
        if (request.url.indexOf(environment.apiUrl) > -1) {
            return this.getTokenConfig(false, next, request)
                .pipe(
                    catchError((error: HttpErrorResponse) => {
                        this.tableStorageService.loggerErrorHttp(error)
                        if (error && error.status === 403) {
                            return this.getTokenConfig(true, next, request)
                        }
                        return throwError(error);
                    })
                )
        }
        return next.handle(request);
    }

    private getTokenConfig(isExpired: boolean, next: HttpHandler, request: HttpRequest<any>): Observable<HttpEvent<any>> {
        return this.tableStorageService
            .getToken$(isExpired)
            .pipe(
                mergeMap(token => next.handle(
                    this.addAuthenticationToken(token, request)
                )),
            )
    }
    private addAuthenticationToken(token: string, request: HttpRequest<any>): HttpRequest<any> {
        const tokenData = token.slice(1, token.length)
        let headers: { [name: string]: string | string[] } = {
            ['Content-Type']: 'application/json;odata=nometadata',
            ['Accept']: 'application/json;odata=nometadata'
        }
        // TODO VALIDATE URL EXTERN API
        if (request.url == environment.apiUrlToken) {
            return request;
        }
        let url = `${request.url}&${tokenData}`
        if (request.method === 'POST' || request.method === 'PUT') {
            url = `${request.url}?${tokenData}`
        }
        else if(request.method === 'DELETE'){
            url = `${request.url}?${tokenData}`
            headers = {
                ...headers,
                ['If-Match']: '*'
            }
        }
        return request.clone({
            url,
            setHeaders: headers
        });
    }
}