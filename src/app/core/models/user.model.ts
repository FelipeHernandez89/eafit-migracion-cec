export interface UserModel {
    uid: string;
    photoURL?: string;
    fullName: string;
    firstName: string;
    lastName: string;
    email: string;
}

export interface UserGraphApiModel {
    id?: string,
    objectId?: string,
    businessPhones?: Array<string>,
    displayName: string,
    givenName?: string,
    jobTitle?: string,
    mail?: string,
    mobilePhone?: string,
    officeLocation?: string,
    preferredLanguage?: string,
    surname?: string,
    userPrincipalName?: string,
    imageProfile: string,
    Groups?: any[],
    Tags?: any[]
}