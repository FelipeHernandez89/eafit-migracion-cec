export interface MenuItemModel {
    name?: string;
    route?: string[];
    icon?: string;
    children?: Array<MenuItemModel>;
    action?: Function;
}