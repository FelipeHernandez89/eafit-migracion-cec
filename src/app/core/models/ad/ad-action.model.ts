import { AD } from '.';

export interface ADActionModel {
    queryParams: string // ListConfiguration(PartitionKey='Address',RowKey='1')
    payload?: AD
}