export interface DocumentModel {
    RowKey: string,
    PartitionKey: string,
    Timestamp: string,
    Description: string,
    
    // Documentos
    DocumentName: string,
    IdCasefile: string,
    Casefile: string,
    IdDocumentalType: string,
    DocumentalType: string,
    IdWorkflow: string,
    Workflow: string,
    DocumentDescription: string,

    //Entrada
    IdSender: string,
    SenderName: string,
    SenderAddress: string,
    SenderState: string,
    SenderPhone: string,
    SenderMail: string,

    IdReceptor: string,
    ReceptorName: string,
    ReceptorMail: string,
    ReceptorAddress: string,
    ReceptorState: string,
    ReceptorPhone: string;

    HasReturn: string,
    Settled: string,

    Closed: boolean;
    ClosedDate: string;
    UserClosed: string;
    IdUserClosed: string,
    EmailUserClosed: string,
    Active: boolean
    /* ReceptorMail */

    //Interno
    // ReceptorName:string;
    // ReceptorMail:string
    // SenderName:string
    // SenderMailstring
    
}