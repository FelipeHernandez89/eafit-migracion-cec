export interface CasefileModel {
  action: any;
  RowKey: string;
  PartitionKey: string;
  Timestamp?: string,
  ModifiedDateArchive: string;
  ModifiedDate: string;

  // CreatedDate: string;
  // IdOwner: string,
  // EmailOwner: string,
  // Owner: string;

  IdInitialState: string;
  InitialState: string;
  IdFinalState: string;
  FinalState: string;
  Automatic: Boolean;
  Time: string;

  IdOrganizationalUnit: string;
  OrganizationalUnit: string

  IdStorageUnit: string;
  StorageUnit: string;

  IdSerie: string;
  Serie: string

  IdSubserie: string;
  Subserie: string

  IdStatusCasefile: string;
  StatusCasefile: string;

  IdArchive: string;
  Archive: string


  Title: string;


  Responsable: string;
  IdResponsable: string,
  EmailResponsable: string,

  Keywords: any;

  //closed
  Closed: boolean;
  ClosedDate: string;
  UserClosed: string;
  IdUserClosed: string,
  EmailUserClosed: string,
  Active: boolean
}