export interface ConfidentialDocModel {
    RowKey: string,
    PartitionKey: string,
    Timestamp: string,

    Filed:string,//Radicado
    FiledDate:string,

    IdOwner:string,
    Owner:string,

    CreatedDate:string,
    DocumentName: string,
    IdStorageUnit: string;
    StorageUnit: string;
    DocumentDescription: string,

    IdDocumentalType: string,
    DocumentalType: string,
    IdWorkflow: string,
    Workflow: string,

    //Entrada
    IdSender: string,
   /*  SenderName: string, */
    SenderAddress: string,
    SenderState: string,
    SenderPhone: string,
    SenderMail:string,
    /* SenderMail: string, */
    ReceptorName: string,
    ReceptorMail: string,
    HasReturn: string,
    Settled: string,

    //Salida
    SenderName: string,
    IdReceptor: string,
    /* ReceptorName */
    ReceptorAddress: string,
    ReceptorState: string,
    ReceptorPhone: string;

    Closed: boolean;
    ClosedDate: string;
    UserClosed: string;
    IdUserClosed: string,
    EmailUserClosed: string,
    Active: boolean
}