import { CasefileModel } from './casefile.model';
import { DocumentModel } from './document.model';
import { ConfidentialDocModel } from './confidentialDoc.model';



export type ManagementModel = CasefileModel |
                              DocumentModel |
                              ConfidentialDocModel
