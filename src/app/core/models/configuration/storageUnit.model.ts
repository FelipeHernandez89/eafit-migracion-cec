export interface StorageUnitsModel {
    IdStorageUnit: string;
    Name: string;
    Location: string;
    Dimensions: string;
    Level: string;
    IdParent: string;
    Active: boolean;
    PartitionKey: string;
    RowKey: string;
    Description : string;
    Timestamp : string;
}