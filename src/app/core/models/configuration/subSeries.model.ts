export interface SubSeriesModel {
    IdsDisposicionFinal : string,
    IdsFileEvaluation : string,
    Name : string,
    Description : string,
    Keycode : string,
    IdOrganizationalUnit : string,
    IdSerie: string,
    Active : boolean,
    PartitionKey : string,
    RowKey : string,
    Timestamp : string;
}