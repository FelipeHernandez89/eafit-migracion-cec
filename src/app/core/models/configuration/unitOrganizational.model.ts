export interface UnitOrganizationalModel {
    Name : string,
    IdParent : string,
    Level: string,
    IdOrganizationType : string,
    Active : boolean,
    PartitionKey : string,
    RowKey : string,
    Description : string;
    Timestamp : string;   
}