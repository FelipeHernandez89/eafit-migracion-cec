export interface ConfigSettledModel {
    Name : string,
    Description : string,
    /* Keycode : string,
    StorageSize : string,
    IdStorageUnity : string, */
    autoIncrement : string,
    currentConsecutive : string,
    dateRestart: string,
    formatDate: string,
    initialSettled : string,
    maxSettled : string,
    Workflow : string,
    Active : boolean,
    PartitionKey : string,
    RowKey : string,
    Timestamp : string
}