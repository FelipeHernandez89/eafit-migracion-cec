export interface DocumentaryModel {
    Description: string,
    Name: string,
    Keycode: string,
    Active: boolean,
    PartitionKey: string,
    RowKey: string,
    Timestamp: string,
    ETag: string
}