import { DispositionModel } from './disposition.model';
import { DocumentaryModel } from './documentary.model';
import { FileValuesModel } from './file-value.model';
import { DocumentaryTypesModel } from './documentaryType.model';
import {WorkFlowsModel} from './workFlow.model'


export type ConfigurationModel = DispositionModel
    | DocumentaryModel
    | FileValuesModel
    | DocumentaryTypesModel
    | WorkFlowsModel
    
