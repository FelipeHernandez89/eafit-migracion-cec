export interface SeriesModel {
    Name : string,
    Description : string,
    Keycode : string,
    IdOrganizationalUnit : string,
    Active : boolean,
    PartitionKey : string,
    RowKey : string,
    Timestamp : string
}