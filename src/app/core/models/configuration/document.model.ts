export interface DocumentsModel {
    RowKey: string,
    PartitionKey: string,
    Timestamp: string,
    Description: string,
    
    // Documentos
    DocumentName: string,
    IdCasefile: string,
    Casefile: string,
    IdDocumentalType: string,
    DocumentalType: string,
    IdWorkflow: string,
    Workflow: string,
    DocumentDescription: string,

    //Entrada
    IdSender: string,
   /*  SenderName: string, */
    SenderAddress: string,
    SenderState: string,
    SenderPhone: string,
    /* SenderMail: string, */
    ReceptorName: string,
    ReceptorMail: string,
    HasReturn: string,
    Settled: string,

    //Salida
    SenderName
    SenderMail
    IdReceptor
    /* ReceptorName */
    ReceptorAddress
    ReceptorState
    ReceptorPhone
    /* ReceptorMail */

    //Interno
    /* ReceptorName
    ReceptorMail
    SenderName
    SenderMail
 */
    
}