export interface StatesTransactionModel {
    Name:string;
    IdStateTransaction: string;
    IdInitialState: string;
    IdFinalState: string;
    IdOrganizationalUnit: string,
    IdSerie: string,
    IdSubserie: string,
    Time: number;
    Automatic: boolean;
    Active: boolean;
    PartitionKey: string;
    RowKey: string;
    Description : string;
    Timestamp : string;
}