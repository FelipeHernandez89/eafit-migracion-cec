export interface AgendaModel {
    Name : string,
    Description : string,
    Identificacion: string,
    Nombre: string,
    Direccion: string,
    Municipio: string,
    Telefono: string,
    CorreoElectronico: string,
    
    Keycode : string,
    IdOrganizationalUnit : string,
    Active : boolean,
    PartitionKey : string,
    RowKey : string,
    Timestamp : string
    
}