export interface DispositionFinalsModel {
    Description: string,
    Name: string,
    Active: boolean,
    Keycode: string,
    PartitionKey: string,
    RowKey: string,
    Timestamp: string,
    ETag: string
}