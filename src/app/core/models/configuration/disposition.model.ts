export interface DispositionModel {
    Description : string,
    Name : string,
    Active : boolean,
    PartitionKey : string,
    RowKey : string,
    Timestamp? : string,
    ETag? : string
}