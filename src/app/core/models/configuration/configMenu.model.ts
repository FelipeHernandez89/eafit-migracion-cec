export interface ConfigMenuModel {
    Name : string,
    Description : string,
    Keycode : string,
    StorageSize : string,
    IdStorageUnity : string,
    Active : boolean,
    PartitionKey : string,
    RowKey : string,
    Timestamp : string
}