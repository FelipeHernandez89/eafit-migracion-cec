import { ConfigurationModel } from '@models/configuration';
import { ManagementModel } from '@models/management';

export type TablesStorages = ConfigurationModel | ManagementModel