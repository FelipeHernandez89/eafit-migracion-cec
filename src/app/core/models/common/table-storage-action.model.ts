import { TablesStorages } from '.';

export interface TableStorageActionModel{
    queryParams: string // ListConfiguration(PartitionKey='Address',RowKey='1')
    payload?: TablesStorages
}