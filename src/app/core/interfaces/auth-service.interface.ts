import { Observable } from 'rxjs';

import { UserModel } from "@models/user.model";
import { Authenticate } from '@models/Authenticate.model';

export interface AuthServiceInterface {
    login$(credential: Authenticate): Observable<UserModel>;
    createUser$(user: UserModel): Observable<UserModel>;
    logout$(): Observable<boolean>
}