import { TableStorageActionModel } from '@models/common/table-storage-action.model';
import { Observable } from 'rxjs';
import { TablesStorages } from '@models/common';
import { HttpErrorResponse } from '@angular/common/http';

export interface TableStorageServiceInterface {
    getToken$(isExpired: boolean): Observable<string>;
    create$(action: TableStorageActionModel): Observable<TablesStorages>;
    search$(action: TableStorageActionModel): Observable<TablesStorages[]>;
    update$(action: TableStorageActionModel): Observable<TablesStorages>;
    delete$(action: TableStorageActionModel): Observable<TablesStorages>;
    loggerErrorHttp(error: HttpErrorResponse): void;
    
}