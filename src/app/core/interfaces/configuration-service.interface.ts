import { Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { MenuItemModel } from '@models/menu-item.model';

export interface ConfigurationServiceInterface {
    getMenuItems$(): Observable<MenuItemModel[]>;
    getNavBarOptions$(): Observable<MenuItemModel[]>;
}