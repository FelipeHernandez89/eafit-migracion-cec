import { Pipe, PipeTransform } from "@angular/core";
import { getLanguageLibrary } from "../../internationalization/languagelibrary";

@Pipe({
  name: "translate"
})
export class TranslatePipe implements PipeTransform {
  transform(value: any): any {
    return getLanguageLibrary()[value] ? getLanguageLibrary()[value] : value;
  }
}
