import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatFileContainer'
})
export class FormatFileContainerPipe implements PipeTransform {

  transform(value: string): any {
    return value.toLowerCase().replace(/[^a-z0-9]/g, '');
  }

}
