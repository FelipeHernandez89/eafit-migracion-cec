import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SmartAsyncPipe } from './smart-async/smart-async.pipe';
import { OrderArrayPipe } from './order-array/order-array.pipe';
import { KeysPipe } from './keys/keys.pipe';
import { ArrayFilterPipe } from './array-filter/array-filter.pipe';
import { ErrorMessagePipe } from './error-message/error-message.pipe';
import { TranslatePipe } from './translate/translate.pipe';
import { FormatFileContainerPipe } from './format-file-container/format-file-container.pipe';

const PIPES = [
  FormatFileContainerPipe,
  SmartAsyncPipe,
  OrderArrayPipe,
  KeysPipe,
  ArrayFilterPipe,
  ErrorMessagePipe,
  TranslatePipe,
];

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ...PIPES,
  ]
  , exports: [
    ...PIPES
  ]
})
export class PipesModule { }
