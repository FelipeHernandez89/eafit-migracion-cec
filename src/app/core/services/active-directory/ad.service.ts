import { Injectable } from '@angular/core';
import { ADServiceInterface } from '@interfaces/ad-service.interface';
import { ADActionModel } from '@models/ad/ad-action.model';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { environment } from '@environments/environment';
import { TOKEN_GRAPH_API } from '@root/core/constants/ad.constants';
import { AD } from '@models/ad';

@Injectable({
  providedIn: 'root'
})
export class AdService implements ADServiceInterface {

  search$(action: ADActionModel): Observable<AD[]> {
    return this._httpClient
      .get(`${environment.graphApiUrl}${action.queryParams}`)
      .pipe(
        map(data => data["value"] as AD[]),
      )
  }

  getToken$(isExpired: boolean): Observable<string> {
    const token: string = localStorage.getItem(TOKEN_GRAPH_API);
    if (!isExpired && token) return of(token)
    return this._httpClient
      .get(environment.graphApiToken)
      .pipe(
        tap(console.log),
        map(data => data["token"]),
        tap(token => localStorage.setItem(TOKEN_GRAPH_API, token))
      )
  }

  constructor(private _httpClient: HttpClient) { }

}
