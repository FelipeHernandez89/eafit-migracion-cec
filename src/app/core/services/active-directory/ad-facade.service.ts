import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromAppRoot from '@reducers/index';
import { ADActionModel } from '@models/ad/ad-action.model';
import { Tables } from '@root/core/enums/table.enum';
import { AdActions } from '@actions/index';
import { Observable } from 'rxjs';
import { UserGraphApiModel } from '@models/user.model';
import { getADUsers } from '@reducers/ad.reducer';

@Injectable({
  providedIn: 'root'
})
export class AdFacadeService {

  constructor(
    private store: Store<fromAppRoot.State>,
  ) { }

  /**
   * Obtiene los usuarios del directorio activo
   */
  searchADUsers$ = (query) => {
    const action: ADActionModel = {
      queryParams: `${Tables.USERS}?api-version=1.6&$filter=userType eq 'Member' and startswith(displayName,'${query}')`
    }
    this.store.dispatch(AdActions.searchADUsers({ action }));
  }

  getADUsers$ = (): Observable<UserGraphApiModel[]> => this.store.select(fromAppRoot.getADUsers);
}
