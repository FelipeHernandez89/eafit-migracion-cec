import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { TableStorageServiceInterface } from '@interfaces/table-storage.interface';
import { TableStorageActionModel } from '@models/common/table-storage-action.model';
import { Observable, of } from 'rxjs';
import { environment } from '@environments/environment';
import { map, tap } from 'rxjs/operators';
import { TablesStorages } from '@models/common';

@Injectable({
  providedIn: 'root'
})
export class TableStorageService implements TableStorageServiceInterface {

  constructor(private httpClient: HttpClient) { }

  create$(action: TableStorageActionModel): Observable<TablesStorages> {
    return this.httpClient
      .post(`${environment.apiUrl}${action.queryParams}`, action.payload)
      .pipe(
        map(data => data as TablesStorages)
      )
  }
  delete$(action: TableStorageActionModel): Observable<TablesStorages> {
    return this.httpClient
      .delete(`${environment.apiUrl}${action.queryParams}`)
      .pipe(
        map(data => action.payload)
      )
  }
  search$(action: TableStorageActionModel): Observable<TablesStorages[]> {
    return this.httpClient
      .get(`${environment.apiUrl}${action.queryParams}`)
      .pipe(
        map(data => data["value"] as TablesStorages[]),
      )
  }

  update$(action: TableStorageActionModel): Observable<TablesStorages> {
    return this.httpClient
      .put(`${environment.apiUrl}${action.queryParams}`, action.payload)
      .pipe(
        map(data => action.payload)
      )
  }
  /* delete$(action: TableStorageActionModel): Observable<boolean> {
    throw new Error("Method not implemented.");
  } */
  /* delete$(action: TableStorageActionModel): Observable<TablesStorages> {
    return this.httpClient
      .put(`${environment.apiUrl}${action.queryParams}`, action.payload)
      .pipe(
        map(data => action.payload)
      )
  } */
  getToken$(isExpired: boolean): Observable<string> {
    const token: string = localStorage.getItem('token');
    // TODO DESCOMENTAR PARA VALIDAR EL CASO DEL TOKEN VENCIDO, SERA ACTUALIZADO AUTOMATICAMENTE DESDE EL INTECEPTOR
    // const token: string = "?sv=2015-12-11&sig=E6YP6MT4rAhW2mYZuk1L2Weye%2BW9xGOI3MOlIA0Pu3I%3D&spr="
    if (!isExpired && token) return of(token)
    return this.httpClient
      .get(environment.apiUrlToken)
      .pipe(
        map(data => data["token"]),
        tap(token => localStorage.setItem("token", token))
      )
  }

  loggerErrorHttp(error: HttpErrorResponse): void {
    console.log("Error", error.message);
  }

}
