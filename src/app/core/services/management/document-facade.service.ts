import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromAppRoot from '@reducers/index';
import { Observable } from 'rxjs';
import { DocumentModel } from '@models/management/Document.model';
import { TableStorageActionModel } from '@models/common/table-storage-action.model';
import { Tables } from '@root/core/enums/table.enum';
import { DocumentActions } from '@actions/index';

@Injectable({
  providedIn: 'root'
})
export class DocumentFacadeService {

  getDocuments$(): Observable<DocumentModel[]> {
    return this.store
      .select(fromAppRoot.getDocuments)
  }
  getRecentlyCreatedDocument$(): Observable<DocumentModel> {
    return this.store
      .select(fromAppRoot.getRecentlyCreatedDocument);
  }
  searchDocuments$(): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.DOCUMENT}?`
    }
    this.store.dispatch(DocumentActions.searchDocuments({ action }));
  }
  DocumentSender$(SenderName): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.DOCUMENT}?$filter=SenderName eq '${SenderName}'`
    }
    this.store.dispatch(DocumentActions.searchDocuments({ action }));
  }
  
  DocumentRecept$(ReceptorName): void {
    const action: TableStorageActionModel = {
      
      queryParams: `${Tables.DOCUMENT}?$filter=ReceptorName eq '${ReceptorName}'`
    }
    this.store.dispatch(DocumentActions.searchDocuments({ action }));
  }
  createDocument(payload: DocumentModel): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.DOCUMENT}`,
      payload
    }
    this.store.dispatch(DocumentActions.createDocument({ action }));
  }
  updateDocument(payload: DocumentModel): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.DOCUMENT}(PartitionKey='${payload.PartitionKey}',RowKey='${payload.RowKey}')`,
      payload
    }
    this.store.dispatch(DocumentActions.updateDocument({ action }));
  }
  selectDocument(Document: DocumentModel): void {
    this.store.dispatch(DocumentActions.selectDocument({ Document }));
  }
  deleteDocument(payload: DocumentModel): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.DOCUMENT}(PartitionKey='',RowKey='${payload.RowKey}')`,
      payload
    }
    this.store.dispatch(DocumentActions.deleteDocument({ action }));
  }
  constructor(
    private store: Store<fromAppRoot.State>,
  ) { }



}
