import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromAppRoot from '@reducers/index';
import { Observable } from 'rxjs';
import { CasefileModel } from '@models/management/casefile.model';
import { TableStorageActionModel } from '@models/common/table-storage-action.model';
import { Tables } from '@root/core/enums/table.enum';
import { CasefileActions } from '@actions/index';

@Injectable({
  providedIn: 'root'
})
export class CasefileFacadeService {

  getCasefiles$(): Observable<CasefileModel[]> {
    return this.store
      .select(fromAppRoot.getCasefiles)
  }
  getCurrentCasefile$(): Observable<CasefileModel> {
    return this.store
      .select(fromAppRoot.getCurrentCasefile);
  }
  searchCasefiles$(): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.CASEFILES}?`
    }
    this.store.dispatch(CasefileActions.searchCasefiles({ action }));
  }
  createCasefile(payload: CasefileModel): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.CASEFILES}`,
      payload
    }
    this.store.dispatch(CasefileActions.createCasefile({ action }));
  }
  updateCasefile(payload: CasefileModel): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.CASEFILES}(PartitionKey='${payload.PartitionKey}',RowKey='${payload.RowKey}')`,
      payload
    }
    this.store.dispatch(CasefileActions.updateCasefile({ action }));
  }
  selectCasefile(casefile: CasefileModel): void {
    this.store.dispatch(CasefileActions.selectCasefile({ casefile }));
  }
  deleteCasefile(payload: CasefileModel): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.CASEFILES}(PartitionKey='',RowKey='${payload.RowKey}')`,
      payload
    }
    this.store.dispatch(CasefileActions.deleteCasefile({ action }));
  }
  historicalCaseFile$(){
    const action: TableStorageActionModel = {
      queryParams: `${Tables.CASEFILES}?$filter=Archive eq 'Histórico'`
    }
    this.store.dispatch(CasefileActions.searchCasefiles({ action }));
  }
  centralCaseFile$(){
    const action: TableStorageActionModel = {
      queryParams: `${Tables.CASEFILES}?$filter=Archive eq 'Central'`
    }
    this.store.dispatch(CasefileActions.searchCasefiles({ action }));
  }
  managementCaseFile$(){
    const action: TableStorageActionModel = {
      queryParams: `${Tables.CASEFILES}?$filter=Archive eq 'Gestión'`
    }
    this.store.dispatch(CasefileActions.searchCasefiles({ action }));
  }
  constructor(
    private store: Store<fromAppRoot.State>,
  ) { }



}
