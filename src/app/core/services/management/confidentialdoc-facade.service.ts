import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromAppRoot from '@reducers/index';
import { Observable } from 'rxjs';
import { TableStorageActionModel } from '@models/common/table-storage-action.model';
import { Tables } from '@root/core/enums/table.enum';
import { ConfidentialDocsActions } from '@actions/index';
import { ConfidentialDocModel } from '@models/management/confidentialDoc.model';

@Injectable({
  providedIn: 'root'
})
export class ConfidentialDocumentFacadeService {

  getDocuments$(): Observable<ConfidentialDocModel[]> {
    return this.store
      .select(fromAppRoot.getConfidentialDocs)
  }
  getRecentlyCreatedDocument$(): Observable<ConfidentialDocModel> {
    return this.store
      .select(fromAppRoot.getRecentlyCreatedConfidentialDoc);
  }
  searchDocuments$(): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.CONFIDENTIAL_DOCUMENTS}?`
    }
    this.store.dispatch(ConfidentialDocsActions.searchConfidentialDocs({ action }));
  }
  createDocument(payload: ConfidentialDocModel): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.CONFIDENTIAL_DOCUMENTS}`,
      payload
    }
    this.store.dispatch(ConfidentialDocsActions.createConfidentialDoc({ action }));
  }
  updateDocument(payload: ConfidentialDocModel): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.CONFIDENTIAL_DOCUMENTS}(PartitionKey='${payload.PartitionKey}',RowKey='${payload.RowKey}')`,
      payload
    }
    this.store.dispatch(ConfidentialDocsActions.updateConfidentialDoc({ action }));
  }
  selectDocument(ConfidentialDoc: ConfidentialDocModel): void {
    this.store.dispatch(ConfidentialDocsActions.selectConfidentialDoc({ ConfidentialDoc }));
  }
  deleteDocument(payload: ConfidentialDocModel): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.CONFIDENTIAL_DOCUMENTS}(PartitionKey='',RowKey='${payload.RowKey}')`,
      payload
    }
    this.store.dispatch(ConfidentialDocsActions.deleteConfidentialDoc({ action }));
  }
  constructor(
    private store: Store<fromAppRoot.State>,
  ) { }



}
