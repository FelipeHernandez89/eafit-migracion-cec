import { Injectable } from '@angular/core';
import * as fromAppRoot from '@reducers/index';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ManagementFacadeService {

  constructor(
    private store: Store<fromAppRoot.State>,

  ) { }

  getIsLoading$(): Observable<boolean> {
    return this.store
      .select(fromAppRoot.isLoadingManagement);
  }

  getMessages$() {
    return this.store.select(fromAppRoot.getMessageManagement);
  }

}
