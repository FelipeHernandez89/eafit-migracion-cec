import { Injectable } from '@angular/core';
import * as fromAppRoot from '@reducers/index';
import { Store } from '@ngrx/store';
import { ViewsActions } from '@actions/index';
import { TableStorageActionModel } from '@models/common/table-storage-action.model';
import { Tables } from '@root/core/enums/table.enum';

@Injectable({
    providedIn: 'root'
})
export class ViewsFacadeService {
    myConfidentials = this.store.select(fromAppRoot.getMyConfidentials);

    constructor(
        private store: Store<fromAppRoot.State>,
    ) { }

    getMyConfidentials$(payload) {
        const action: TableStorageActionModel = {
            queryParams: `${Tables.CASEFILES}`,
            payload
        }
        this.store.dispatch(ViewsActions.getMyConfidentialsAction({ action }));
    }

}
