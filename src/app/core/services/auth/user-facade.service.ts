import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as fromAppRoot from '@reducers/index';
import { AuthActions } from '@actions/index';
import { UserModel } from '@models/user.model';
import { Authenticate } from '@models/Authenticate.model';

@Injectable({
    providedIn: 'root'
})
export class UserFacadeService {
    constructor(
        private store: Store<fromAppRoot.State>,
    ) { }

    getErrorLogin$(): Observable<string> {
        return this.store
            .select(fromAppRoot.getLoginPageError);
    }
    getCurrentUser$(): Observable<UserModel> {
        return this.store.select(fromAppRoot.getUser)
    }
    isLoggedIn$(): Observable<boolean> {
        return this.store.select(fromAppRoot.getLoggedIn);
    }
    isLoadingAuth$(): Observable<boolean> {
        return this.store.select(fromAppRoot.getAuthLoading);
    }
    loginWithCredential(credential: Authenticate) {
        this.store.dispatch(AuthActions.login({ credential }));
    }
    createUser(user: UserModel) {
        this.store.dispatch(AuthActions.createUser({ user }))
    }
    logout() {
        this.store.dispatch(AuthActions.logout());
    }
}