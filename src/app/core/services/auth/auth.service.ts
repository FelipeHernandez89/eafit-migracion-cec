import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';


import { AuthServiceInterface } from '@interfaces/auth-service.interface';
import { UserModel } from '@models/user.model';
import { emptyUser } from '@utilities/data.util';
import { Authenticate } from '@models/Authenticate.model';

@Injectable()
export class AuthService implements AuthServiceInterface {
    createUser$(user: UserModel): Observable<UserModel> {
        console.log("Create User", user);
        return of(user);
    }

    login$(credential: Authenticate): Observable<UserModel> {
        return of(emptyUser());
    }

    logout$(): Observable<boolean> {
        return of(true);
    }
}