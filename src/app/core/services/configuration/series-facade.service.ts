import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SeriesModel } from '@models/configuration/series.model';
import { Store } from '@ngrx/store';
import * as fromAppRoot from '@reducers/index';
import { SeriesAction } from '@actions/index';
import { TableStorageActionModel } from '@models/common/table-storage-action.model';
import { Partitions } from '@root/core/enums/partition.enum';
import { Tables } from '@root/core/enums/table.enum';

@Injectable({
  providedIn: 'root'
})
export class SeriesFacadeService {

  getSeries$(): Observable<SeriesModel[]> {
    return this.store
      .select(fromAppRoot.getSeries)
  }
  getCurrentSeries$(): Observable<SeriesModel> {
    return this.store
      .select(fromAppRoot.getCurrentSeries);
  }
  searchSeries$(): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.CONFIGURATIONS}?$filter=PartitionKey eq '${Partitions.SERIES}'`
    }
    this.store.dispatch(SeriesAction.searchSeries({ action }));
  }
  createSeries(payload: SeriesModel): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.CONFIGURATIONS}`,
      payload
    }
    this.store.dispatch(SeriesAction.createSeries({ action }));
  }
  updateSeries(payload: SeriesModel): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.CONFIGURATIONS}(PartitionKey='${payload.PartitionKey}',RowKey='${payload.RowKey}')`,
      payload
    }
    this.store.dispatch(SeriesAction.updateSeries({ action }));
  }
  selectSeries(Series: SeriesModel): void {
    this.store.dispatch(SeriesAction.selectSeries({ Series }));
  }
  deleteSeries(payload: SeriesModel): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.CONFIGURATIONS}(PartitionKey='${payload.PartitionKey}',RowKey='${payload.RowKey}')`,
      payload
    }
    this.store.dispatch(SeriesAction.deleteSeries({ action }));
  }
  constructor(
    private store: Store<fromAppRoot.State>,
  ) { }
}
