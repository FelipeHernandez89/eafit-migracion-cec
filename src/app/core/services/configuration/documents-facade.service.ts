/* import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DocumentsModel } from '@models/configuration/document.model';
import { Store } from '@ngrx/store';
import * as fromAppRoot from '@reducers/index';
import { DocumentActions } from '@actions/index';
import { TableStorageActionModel } from '@models/common/table-storage-action.model';
import { Partitions } from '@root/core/enums/partition.enum';
import { Tables } from '@root/core/enums/table.enum';
import { CasefileModel } from '@models/management/casefile.model';
import { DocumentModel } from '@models/management/document.model';

@Injectable({
  providedIn: 'root'
})
export class DocumentsFacadeService {

  getDocuments$(): Observable<DocumentsModel[]> {
    return this.store
      .select(fromAppRoot.getDocuments)
  }
  getCurrentDocuments$(): Observable<DocumentsModel> {
    return this.store
      .select(fromAppRoot.getCurrentDocuments);
  }
  searchDocuments$(): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.DOCUMENT}?`
    }
    this.store.dispatch(DocumentActions.searchDocuments({ action }));
  }

  DocumentSender$(SenderName): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.DOCUMENT}?$filter=SenderName eq '${SenderName}'`
    }
    this.store.dispatch(DocumentActions.searchDocuments({ action }));
  }
  
  DocumentRecept$(ReceptorName): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.DOCUMENT}?$filter=ReceptorName eq '${ReceptorName}'`
    }
    this.store.dispatch(DocumentActions.searchDocuments({ action }));
  }

  searchDocumentsByCaseFile$(id:string): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.DOCUMENT}?api-version=1.6&$filter=IdCasefile eq '${id}'`
    }
    this.store.dispatch(DocumentActions.searchDocuments({ action }));
  }
  createDocuments(payload: DocumentsModel): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.DOCUMENT}`,
      payload
    }
    this.store.dispatch(DocumentActions.createDocument({ action }));
  }
  updateDocuments(payload: DocumentsModel): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.DOCUMENT}(PartitionKey='${payload.PartitionKey}',RowKey='${payload.RowKey}')`,
      payload
    }
    this.store.dispatch(DocumentActions.updateDocuments({ action }));
  }
  selectDocuments(Documents: DocumentsModel): void {
    this.store.dispatch(DocumentActions.selectDocuments({ Documents }));
  }
  deleteDocuments(payload: DocumentsModel): void {
    const action: TableStorageActionModel = {
      queryParams: `${Tables.DOCUMENT}(PartitionKey='',RowKey='${payload.RowKey}')`,
      payload
    }
    this.store.dispatch(DocumentActions.deleteDocuments({ action }));
  }
  constructor(
    private store: Store<fromAppRoot.State>,
  ) { }
}
 */