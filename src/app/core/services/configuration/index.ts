import { ConfigurationService } from '@services/configuration/configuration.service';
import { ConfigurationFacadeService } from '@services/configuration/configuration-facade.service';
import { SeriesFacadeService } from '@services/configuration/series-facade.service';
//import { DocumentsFacadeService } from './documents-facade.service';

export {
    ConfigurationService,
    ConfigurationFacadeService,
    SeriesFacadeService,
    //DocumentsFacadeService,
}
