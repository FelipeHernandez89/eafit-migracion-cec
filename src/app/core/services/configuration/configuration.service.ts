import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ConfigurationServiceInterface } from '@interfaces/configuration-service.interface';
import { MenuItemModel } from '@models/menu-item.model';
import { menuItemsData, navBarOptions } from '@utilities/data.util';

@Injectable()
export class ConfigurationService implements ConfigurationServiceInterface {
    constructor() { }

    getMenuItems$(): Observable<MenuItemModel[]> {
        return of(menuItemsData())
    }

    getNavBarOptions$(): Observable<MenuItemModel[]> {
        return of(navBarOptions())
    }
    
}