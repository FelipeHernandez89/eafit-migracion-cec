import { Injectable, Inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as fromAppRoot from '@reducers/index';
import { ConfigurationServiceInterface } from '@interfaces/configuration-service.interface';
import { MenuItemModel } from '@models/menu-item.model';


@Injectable({
    providedIn: 'root'
})
export class ConfigurationFacadeService {

    getMenuItems$(): Observable<MenuItemModel[]> {
        return this.configurationService
            .getMenuItems$()
    }

    getNavBarOptions$(): Observable<MenuItemModel[]> {
        return this.configurationService
            .getNavBarOptions$()
    }
    getError$(): Observable<string | null> {
        return this.store
            .select(fromAppRoot.getErrorConfiguration);
    }
    getIsLoading$(): Observable<boolean> {
        return this.store
            .select(fromAppRoot.isLoadingConfiguration);
    }
    getMessages$(): Observable<string> {
        return this.store
            .select(fromAppRoot.getMessageConfiguration);
    }
    constructor(
        private store: Store<fromAppRoot.State>,
        @Inject('ConfigurationService') private configurationService: ConfigurationServiceInterface,
    ) { }

}