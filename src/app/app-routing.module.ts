import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


export const routes: Routes = [
  { path: "", redirectTo: "/home", pathMatch: "full" },
  {
    path: "home",
    loadChildren: "./home/home.module#HomeModule"
  },
  {
    path: "auth",
    loadChildren: "./auth/auth.module#AuthModule"
  },
  {
    path: "configuration",
    loadChildren: "./configuration/configuration.module#ConfigurationModule"
  },
  {
    path: "views",
    loadChildren: "./views/views.module#ViewModule"
  },
  {
    path: "management",
    loadChildren: "./management/management.module#ManagementModule"
  },
  { path: "**", redirectTo: "/home" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
