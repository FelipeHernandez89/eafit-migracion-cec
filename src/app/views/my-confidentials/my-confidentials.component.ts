import { Component, OnInit } from '@angular/core';
import { ViewsFacadeService } from '@services/management/views-facade.service';

@Component({
  selector: 'my-confidentials',
  templateUrl: './my-confidentials.component.html',
  styleUrls: ['./my-confidentials.component.scss']
})
export class MyConfidentialsComponent implements OnInit {

  constructor(private ViewsFacade:ViewsFacadeService) { }

  ngOnInit() {
  }

}
