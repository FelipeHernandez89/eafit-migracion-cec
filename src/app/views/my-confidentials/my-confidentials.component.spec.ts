import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyConfidentialsComponent } from './my-confidentials.component';

describe('MyConfidentialsComponent', () => {
  let component: MyConfidentialsComponent;
  let fixture: ComponentFixture<MyConfidentialsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyConfidentialsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyConfidentialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
