import { NgModule } from '@angular/core';
import { PipesModule } from '@root/core/pipes/pipes-module.module';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@root/shared/shared.module';
import { ViewsComponent } from './views.component';
import { ViewRoutingModule } from './views-routing.module';
import { MyConfidentialsComponent } from './my-confidentials/my-confidentials.component';

@NgModule({
  declarations: [
    ViewsComponent,
    MyConfidentialsComponent
  ],
  imports: [
    PipesModule,
    CommonModule,
    SharedModule,
    ViewRoutingModule,
  ],
  providers: []
})
export class ViewModule { }
