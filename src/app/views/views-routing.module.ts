import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewsComponent } from './views.component';
import { MyConfidentialsComponent } from './my-confidentials/my-confidentials.component';


const routes: Routes = [
  {
    path: '',
    component: ViewsComponent,
    children: [
        { path: '', redirectTo: 'myConfidentials', pathMatch: 'full' },
        { path: 'myConfidentials', component: MyConfidentialsComponent },
        { path: '**', redirectTo: '', pathMatch: 'full' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewRoutingModule { }
