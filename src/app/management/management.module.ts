import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManagementRoutingModule } from './management-routing.module';
import { ManagementComponent } from './management.component';
/* import { CasefileComponent } from './components/casefile/casefile.component'; */
import { SharedModule } from '@root/shared/shared.module';
import { PipesModule } from '@root/core/pipes/pipes-module.module';
import { ControlAutocompleteComponent } from '../shared/common-controls/control-autocomplete/control-autocomplete.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbPaginationModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationDialogComponent } from '@root/shared/confirmation-dialog/confirmation-dialog.component';

@NgModule({
  declarations: [
    ManagementComponent,
    /* CasefileComponent, */
  ],
  imports: [
    PipesModule,
    CommonModule,
    SharedModule,
    ManagementRoutingModule,
    NgbModule,
    NgbPaginationModule,
    NgbAlertModule,
  ],
  entryComponents: [/* ConfirmationDialogComponent */],
  providers: []
})
export class ManagementModule { }
