import {
  Component,
  Input,
  ViewChild,
  EventEmitter,
  Output
} from '@angular/core';
import { MatDrawer, MatTreeNestedDataSource } from '@angular/material';
import { NestedTreeControl } from '@angular/cdk/tree';

import { MenuItemModel } from '@models/menu-item.model';
import { UserModel } from '@models/user.model';
import { emptyUser } from '@utilities/data.util';
import { controlTypes, SpecialFormGroup } from '@utilities/SpecialForms';
import { fieldDataToArray, SpecialFormGenerator } from '@utilities/SpecialFormGenerator';

@Component({
  selector: 'app-container',
  templateUrl: './app-container.component.html',
  styleUrls: ['./app-container.component.scss'],
  host: {
    '(document:click)': 'onClick($event)',
    '(window:resize)': 'onResize($event)'
  },
})
export class AppContainerComponent {
  @Input('menuItems') menuItems: MenuItemModel[] = [];
  @Input('navBarOptions') navBarOptions: MenuItemModel[] = [];
  @Input('user') currentUser: UserModel = emptyUser();

  @Output('optionClick') optionClick: EventEmitter<any> = new EventEmitter();
  @ViewChild('drawer', { static: true }) _drawer: MatDrawer;
  drawerType = '';
  form: SpecialFormGroup;

  treeControl = new NestedTreeControl<MenuItemModel>(node => node.children);
  dataSource = new MatTreeNestedDataSource<MenuItemModel>();
  formFields = fieldDataToArray({
    query: {
      label: "Search",
      required:false,
      data: ['text',, 'search'],
      type: controlTypes.text
    }
  });
  onResize() {
    if (window.innerWidth < 768) {
      this.drawerType = 'over';
    } else {
      this.drawerType = 'side';
    }
  }

  onClick() {
    if (this._drawer && this._drawer.opened) {
      this._drawer.close();
    }
  }

  constructor() {
  }

  hasChild = (_: number, node: MenuItemModel) => !!node.children && node.children.length > 0;

  ngOnInit() {
    this.onResize();
    this.dataSource.data = this.menuItems;
    this.form = SpecialFormGenerator(this.formFields);
    console.log(this.form);
  }

  openDrawer() {
    this._drawer.toggle();
  }
}
