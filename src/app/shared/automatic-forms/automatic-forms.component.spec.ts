import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomaticFormsComponent } from './automatic-forms.component';

describe('AutomaticFormsComponent', () => {
  let component: AutomaticFormsComponent;
  let fixture: ComponentFixture<AutomaticFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutomaticFormsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomaticFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
