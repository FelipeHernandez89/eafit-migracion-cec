import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { controlTypes, SpecialFormGroup } from '@utilities/SpecialForms';


@Component({
  selector: 'automatic-forms',
  templateUrl: './automatic-forms.component.html',
  styleUrls: ['./automatic-forms.component.scss']
})
export class AutomaticFormsComponent implements OnInit {
  @Input('form') form: SpecialFormGroup;
  @Input('cancelText') cancelText: string = 'Limpiar';
  @Input('successText') successText: string = 'Guardar';
  @Input('deleteText') deleteText: string = 'Eliminar';
  @Input('enableButtons') enableButtons: boolean = true;

  @Output('submit') submit: EventEmitter<any> = new EventEmitter();
  @Output('clean') clean: EventEmitter<any> = new EventEmitter();
  @Output('delete') delete: EventEmitter<any> = new EventEmitter();

  controlTypes = controlTypes;
  constructor() { }

  ngOnInit() {
  }

  submitAction() {
    this.form.valid ?
      this.submit.emit(this.form.value) :
      this.form.unpristineRequired();
  }

  cancelAction() {
    this.form.specialReset();
  }

  deleteAction() {
    this.delete.emit(this.form.value)
  }

}
