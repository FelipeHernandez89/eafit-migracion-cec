import { Component, OnInit, Input, ViewChild, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';

import { FieldDescriptor, controlTypes } from '@utilities/SpecialForms';

@Pipe({ name: 'orderTableColumns' })
export class orderTableColumns implements PipeTransform {
  transform(data: FieldDescriptor[]): any {
    return data.map(item => item.name);
  }
}

@Pipe({ name: 'getCellData' })
export class getCellDataPipe implements PipeTransform {
  transform(data: Object, fieldData: FieldDescriptor): any {
    switch (fieldData.type) {
      case controlTypes.pkey: return data[fieldData.name];
      case controlTypes.text: return data[fieldData.name];
      case controlTypes.dropdown: return data[fieldData.name]/* [fieldData.data[0]] */;
      case controlTypes.autocomplete: return data[fieldData.name]/* [fieldData.data[1]] */;
      case controlTypes.longText: return data[fieldData.name];
      case controlTypes.chips: return data[fieldData.name];
      case controlTypes.boolean: return data[fieldData.name] ? fieldData.data[0] : fieldData.data[1];
      case controlTypes.actions: return fieldData.data.join();
    }
  }
}

@Component({
  selector: 'list-table',
  templateUrl: './list-table.component.html',
  styleUrls: ['./list-table.component.scss'],
})
export class ListTableComponent implements OnInit {
  @Input('data') data: any[];
  @Input('structure') structure: FieldDescriptor[];
  @Output('editItem') editItem: EventEmitter<any> = new EventEmitter();
  @Output('action') action: EventEmitter<any> = new EventEmitter();


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  controlTypes = controlTypes;
  dataSource: MatTableDataSource<any>;
  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes.data && changes.data.currentValue) {
      this.dataSource = new MatTableDataSource<any>(this.data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }

  setupFilter(column: string) {
    this.dataSource.filterPredicate = (d, filter: string) => {
      const textToSearch = d[column] && d[column].toLowerCase() || '';
      return textToSearch.includes(filter);
    };
  }

  ngOnInit() { }

  clickedItem(item) {
    this.editItem.emit(item);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}


