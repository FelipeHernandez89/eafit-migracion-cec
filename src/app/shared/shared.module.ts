import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CustomMaterialModule } from '@root/custom-material/custom-material.module';
import { PipesModule } from '@root/core/pipes/pipes-module.module';
import { components } from '.';
import { DeleteDialogComponent } from './delete-dialog/delete-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    CustomMaterialModule,
    PipesModule
  ],
  declarations: [...components],
  exports: [
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    ...components,
    CustomMaterialModule,
  ],
  entryComponents: [DeleteDialogComponent],
})
export class SharedModule { }
