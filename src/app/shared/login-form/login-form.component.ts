import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

import { SpecialFormGroup } from '@utilities/SpecialForms';
import { fieldDataToArray, SpecialFormGenerator } from '@utilities/SpecialFormGenerator';
import { loginForm } from './login-form.form';

@Component({
  selector: 'login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  @Output('sendCredentials') sendCredentials: EventEmitter<any> = new EventEmitter();
  @Output('forgotPass') forgotPass: EventEmitter<any> = new EventEmitter();
  @Output('googleLog') googleLog: EventEmitter<any> = new EventEmitter();
  @Output('goSignUp') goSignUp: EventEmitter<any> = new EventEmitter();

  @Input('serverError') serverError: null | string;
  form: SpecialFormGroup;
  formFields=[];

  constructor() {
  }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.formFields = fieldDataToArray(loginForm());
    this.form = SpecialFormGenerator(this.formFields);
  }

  sendAuthCredentials() {
    this.sendCredentials.emit(this.form.value);
  }
}
