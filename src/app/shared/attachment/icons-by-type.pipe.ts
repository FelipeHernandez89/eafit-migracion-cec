import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'iconsByType'
})
export class IconsByTypePipe implements PipeTransform {
  transform(value: any): any {
    switch (value) {
      case 'image/jpeg': return './assets/icons/image.png';
      default:return './assets/icons/document.png';
    }
  }

}
