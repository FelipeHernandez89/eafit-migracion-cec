import { controlTypes } from '@utilities/SpecialForms';
import { Validators } from '@angular/forms';

export const loginForm = () => Object.assign({
    email: {
      label: "Usuario",
      required:true,
      tooltip:'This is a hint',
      length:30,
      validations:[Validators.email],
      type: controlTypes.text,
      data: ['email'],
    },
    password: {
      label: "Contraseña",
      type: controlTypes.text,
      length:30,
      required:true,
      data:['password']
    },
  });