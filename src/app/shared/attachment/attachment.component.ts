import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

import { SpecialFormGroup } from '@utilities/SpecialForms';
import { AzureStorage } from './scripts/azure-storage.blob.min.js';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { Router } from '@angular/router';
//import { BlobService, UploadConfig, UploadParams } from 'angular-azure-blob-service'
declare const AzureStorage: any;
declare const fileService: any;

@Component({
  selector: 'attachment',
  templateUrl: './attachment.component.html',
  styleUrls: ['./attachment.component.scss']
})
export class AttachmentComponent implements OnInit {

  //En este input se debe ingresar el nombre del BlobContainer que se va a afectar
  //En caso de no existir se crea el contenedor.
  @Input('container') set containerEvent(value: string) {
    if (value) {
      this.container = value;
      this.list();
    }
  }

  container: string

  form: SpecialFormGroup;
  formFields = [];
  blobService;
  blobArray = [];
  files: File[] = [];
  /* account = {
      name: "ronellygestiondocumental",
      sas:  "se=2019-11-20&sp=rwdlac&sv=2018-03-28&ss=b&srt=sco&sig=1TWoryZgnrqT4th/Lkv3sb56uQvQHu6778PvIPXTiz0%3D"
  };

  blobUri = 'https://' + this.account.name + '.blob.core.windows.net';
  this.blobService = AzureStorage.Blob.createBlobServiceWithSas(this.blobUri, this.account.sas); */
  fileData: File = null;
  previewUrl: any = null;
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;
  constructor(
    private http: HttpClient,
    public dialog: MatDialog,
    private _router: Router,
  ) { }

  ngOnInit() {
  }

  getBlobService() {
    const account = {
      //sas: 'se=2019-11-20&sp=rwdlac&sv=2018-03-28&ss=b&srt=sco&sig=1TWoryZgnrqT4th/Lkv3sb56uQvQHu6778PvIPXTiz0%3D',
      sas: localStorage.getItem("token"),
      name: 'ronellygestiondocumental',
    };

    const blobUri = 'https://' + account.name + '.blob.core.windows.net';
    const blobService = AzureStorage.Blob.createBlobServiceWithSas(blobUri, account.sas);
    this.blobService = blobService;
  }

  createContainer() {
    this.getBlobService()
    this.blobService.createContainerIfNotExists(this.container, (error, container) => {
      if (error) {
        // Handle create container error
      } else {
        console.log(container.name);
      }
    });
  }

  fileProgress(event: any) {
    //alert(event.addedFiles)
    //this.fileData = <File>fileInput.target.files[0];
    this.files.push(...event.addedFiles);
    //this.preview();
  }

  async onSubmit(event) {
    this.fileProgress(event)
    this.getBlobService()

    this.blobService.createBlockBlobFromBrowserFile(
      this.container, this.files[0].name, ...event.addedFiles,
      (error, result) => {
        if (error) {
          alert(error)
        } else {
          alert("Archivo agregado correctamente")
          location.reload()
        }
      });


  }

  async list() {
    this.getBlobService()
    this.blobService.listBlobsSegmented(this.container, null, async (error, results) => {
      if (error) {
        // Handle list blobs error
        if (error.code === "ContainerNotFound") this.createContainer();
        alert(error)
      } else {
        this.blobArray = [];
        results.entries.forEach(blob => {
          //alert(blob.name);
          /* var newDiv = document.createElement("h4"); 
          var newContent = document.createTextNode(blob.name); 
          newDiv.appendChild(newContent); //añade texto al div creado. */
          var downloadLink = this.blobService.getUrl(this.container, blob.name,
            (error, result) => {
              if (error) {
                // Handle delete blob error
                //alert(downloadLink)
              } else {
                console.log('Blob download successfully');
              }
            });
          this.blobArray.push({ "blob": blob, "downloadLink": downloadLink + "?sv=2019-02-02&ss=bqtf&srt=sco&sp=rwdlacup&se=2019-11-19T08:42:21Z&sig=PVtnElaCa%2FdTDnRcWWIsMIOi9a3tWVKm%2FL3F63iw4JU%3D&_=1574124295978" })
        });
      }

    });
  }

  Download(link) {
    window.open(link, '_blank')
    // this.getBlobService()
    // var downloadLink = this.blobService.getUrl(this.container, blobName,
    // (error, result) => {
    //   if (error) {
    //       // Handle delete blob error
    //       //alert(downloadLink)
    //   } else {
    //       console.log('Blob download successfully');
    //   }
    // });
    // console.log(downloadLink)
    //alert(downloadLink)
    // this.blobService.getUrl('myshare', 'mydirectory', 'myfile', 'SAS_TOKEN');
    //alert(downloadLink);
  }


  delete(blobEvent) {
    this.getBlobService()
    var blobName = this.container;
    let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      height: '350px',
      width: '550px',
      data: { title: '¿Esta Seguro que desea eliminar?', text: 'Esto no se podra deshacer!!' }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.blobService.deleteBlobIfExists(this.container, blobEvent, (error, result) => {
          if (error) {
            // Handle delete blob error
          } else {
            console.log('Blob deleted successfully');
            location.reload()
          }
        });
      } else {
        //this.cancelCasefile() :
        console.log('dont do it')
      }
      //this.list()
    });

  }

  preview() {
    // Show preview 
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      this.previewUrl = reader.result;
    }
  }

}
