import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlTextAreaComponent } from './control-textarea.component';

describe('ControlTextAreaComponent', () => {
  let component: ControlTextAreaComponent;
  let fixture: ComponentFixture<ControlTextAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlTextAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlTextAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
