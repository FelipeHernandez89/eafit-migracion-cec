import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { SpecialFormControl } from '@utilities/SpecialForms';
import { ENTER, FF_SEMICOLON } from '@angular/cdk/keycodes';
import { MatChipInputEvent, MatChipList } from '@angular/material';
import { Subscription } from 'rxjs';

@Component({
  selector: 'control-chips',
  templateUrl: './control-chips.component.html',
  styleUrls: ['./control-chips.component.scss']
})
export class ControlChipsComponent implements OnInit {

  @Input('sfcontrol') sfcontrol: SpecialFormControl;
  @Output('blurAction') blurAction: EventEmitter<any> = new EventEmitter();
  @Output('enterKey') enterKey: EventEmitter<any> = new EventEmitter();
  @Input('visible') visible = true;
  @Input('selectable') selectable = true;
  @Input('removable') removable = true;
  @Input('addOnBlur') addOnBlur: boolean = true;
  @Output('onAdd') onAdd: EventEmitter<any> = new EventEmitter();
  @Output('onRemove') onRemove: EventEmitter<any> = new EventEmitter();
  @ViewChild('chipList', { static: false }) chipList: MatChipList;
  readonly separatorKeysCodes: number[] = [ENTER, FF_SEMICOLON];
  subs = new Subscription();

  constructor() { }

  ngOnInit() {
    this.subs.add(
      this.sfcontrol.statusChanges.subscribe((status) => {
          this.chipList.errorState = this.sfcontrol.invalid && this.sfcontrol.dirty;
      })
    )
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const newValue = event.value;

    // Add our chip
    if ((newValue || '').trim()) {
      const currentValues = this.sfcontrol.value ? this.sfcontrol.value.split('; ') : [];
      this.sfcontrol.setValue([...currentValues, newValue].join('; '));
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  onBlur(){
    this.sfcontrol.markAsDirty();
    this.chipList.errorState = this.sfcontrol.invalid && this.sfcontrol.dirty;
  }

  remove(value: any[]): void {
    const currentValues = this.sfcontrol.value.split('; ');
    const index = currentValues.indexOf(value);

    if (index > -1) {
      currentValues.splice(index, 1);
      const value = currentValues.join('; ');
      this.sfcontrol.markAsDirty();
      this.sfcontrol.setValue(value === '' ? undefined : value);
    }
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
