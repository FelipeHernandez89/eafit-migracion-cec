import { Component, Input } from '@angular/core';

import { SpecialFormControl } from '@utilities/SpecialForms';

@Component({
  selector: 'control-datepicker',
  templateUrl: './control-datepicker.component.html',
  styleUrls: ['./control-datepicker.component.scss']
})
export class ControlDatepickerComponent {
  @Input('sfcontrol') private _sfcontrol: SpecialFormControl;

  constructor() {
  }

  change(data) {
    this._sfcontrol.setValue(data);
  }
}
