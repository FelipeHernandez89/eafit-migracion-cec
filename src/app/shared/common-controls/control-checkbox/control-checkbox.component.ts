import { Component, Input, Output, EventEmitter } from '@angular/core';

import { SpecialFormControl } from '@utilities/SpecialForms';

@Component({
  selector: 'control-checkbox',
  templateUrl: './control-checkbox.component.html',
  styleUrls: ['./control-checkbox.component.scss'],
})
export class ControlCheckboxComponent {
  @Input('sfcontrol') _sfcontrol: SpecialFormControl;
  @Input('description') _description: string = '';
  @Output('changeEvent') _changeEvent: EventEmitter<any> = new EventEmitter();
}
