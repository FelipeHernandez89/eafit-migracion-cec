import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlMaskedInputComponent } from './control-masked-input.component';

describe('ControlMaskedInputComponent', () => {
  let component: ControlMaskedInputComponent;
  let fixture: ComponentFixture<ControlMaskedInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlMaskedInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlMaskedInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
