import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { SpecialFormControl } from '@utilities/SpecialForms';

@Component({
  selector: 'control-masked-input',
  templateUrl: './control-masked-input.component.html',
  styleUrls: ['./control-masked-input.component.scss']
})
export class ControlMaskedInputComponent implements OnInit {
  @Input('sfcontrol') _sfcontrol: SpecialFormControl;
  @Output('blurAction') _blurAction: EventEmitter<any> = new EventEmitter();
  @Output('enterKey') _enterKey: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() { }

}


