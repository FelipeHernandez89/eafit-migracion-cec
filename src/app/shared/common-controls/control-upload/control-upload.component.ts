import { Component, OnInit, Input } from '@angular/core';
import { SpecialFormControl } from '@utilities/SpecialForms';

@Component({
  selector: 'control-upload',
  templateUrl: './control-upload.component.html',
  styleUrls: ['./control-upload.component.scss']
})
export class ControlUploadComponent implements OnInit {
  @Input('sfcontrol') _sfcontrol: SpecialFormControl;

  constructor(
  ) { }

  ngOnInit() {}

  onSelect(event) {
    this._sfcontrol.setValue([...this._sfcontrol.value, ...event.addedFiles])
  }

  onRemove(event) {
    const currentFiles = this._sfcontrol.value;
    currentFiles.splice(currentFiles.indexOf(event), 1);
    this._sfcontrol.setValue(currentFiles);
  }

}
