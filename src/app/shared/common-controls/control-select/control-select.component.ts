import { Component, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';

import { SpecialFormControl } from '@utilities/SpecialForms';
import { Observable, Subscription, combineLatest } from 'rxjs';
import { tap, filter } from 'rxjs/operators';
import { MatOption, MatSelectChange } from '@angular/material';

@Component({
  selector: 'control-select',
  templateUrl: './control-select.component.html',
  styleUrls: ['./control-select.component.scss']
})
export class ControlSelectComponent {
  @Input('readOnly') _readOnly = false;
  @Input('sfcontrol') _sfcontrol: SpecialFormControl;

  value: any = "";
  subs: Subscription = new Subscription();
  data: Object[] = [];

  ngOnInit() {
    if (this._sfcontrol.data[0] instanceof Observable) {
      this.subs.add(
        combineLatest(
          this._sfcontrol.data[0].pipe(filter(a => a ? true : false)),
          this._sfcontrol.valueChanges,
          (data: Array<any>, value) => {
            const select = data.find(a => a[this._sfcontrol.data[1]] === value);
            select && this._sfcontrol.setComplement(select);
          }
        ).subscribe(() => { })
      )
      this.subs.add(
        this._sfcontrol.data[0].subscribe(d => this.data = d)
      )
    } else {
      this.data = this._sfcontrol.data[0];
      this._sfcontrol.valueChanges.pipe(
      ).subscribe((value) => {
        const select = this.data.find(a => a[this._sfcontrol.data[1]] === value);
        select && this._sfcontrol.setComplement(select);
      })
    }
  }

  select(value) {
    this._sfcontrol.markAsDirty();
    this._sfcontrol.setValue(value);
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
