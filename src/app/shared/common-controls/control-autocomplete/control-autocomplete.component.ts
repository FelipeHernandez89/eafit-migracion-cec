import { Component, Input, Output, EventEmitter, AfterContentInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, filter } from 'rxjs/operators';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import { SpecialFormControl } from '@utilities/SpecialForms';
import { Subscription } from 'rxjs';
@Component({
  selector: 'control-autocomplete',
  templateUrl: './control-autocomplete.component.html',
  styleUrls: ['./control-autocomplete.component.scss'],
})
export class ControlAutocompleteComponent {
  @Input('sfcontrol') _sfcontrol: SpecialFormControl;
  @Output('blurAction') _blurAction: EventEmitter<any> = new EventEmitter();
  @Output('enterKey') _enterKey: EventEmitter<any> = new EventEmitter();

  subs = new Subscription();
  myControl = new FormControl();
  filteredOptions: string[] = [];
  constructor() { }

  ngAfterViewInit() {
    if (this._sfcontrol) {
      if (this._sfcontrol.value instanceof Object) {
        setTimeout(() => {
          this.myControl.setValue(this._sfcontrol.value);
          this._sfcontrol.setValue(this._sfcontrol.value[this._sfcontrol.data[2]]);
        }, 500);
      }

      this.subs.add(
        this._sfcontrol.statusChanges.subscribe((status) => {
          status === 'DISABLED' ? this.myControl.disable() : this.myControl.enable();
          if (this._sfcontrol.pristine && this._sfcontrol.invalid) {
            this.myControl.reset();
          }
        })
      );

      this.subs.add(
        this._sfcontrol.valueChanges.pipe(
          filter((value) => value instanceof Object)
        ).subscribe((value) => {
          this.myControl.setValue(value);
          this._sfcontrol.setValue(value[this._sfcontrol.data[2]]);
        })
      );

      this.subs.add(
        this.myControl.valueChanges.pipe(
          debounceTime(500),
        ).subscribe((query) => {
          this._sfcontrol.data[0](query)
        })
      )

      this.subs.add(
        this._sfcontrol.data[1].subscribe((data) => {
          this.filteredOptions = data;
        })
      )
    }
  }

  configValue = (data) => {
    return (data && data[this._sfcontrol.data[3]]) ? data[this._sfcontrol.data[3]] : '';
  }

  optionSelected(data: MatAutocompleteSelectedEvent) {
    this._sfcontrol.setComplement(data.option.value);
    this._sfcontrol.markAsDirty();
    this._sfcontrol.setValue(data.option.value[this._sfcontrol.data[2]]);
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
