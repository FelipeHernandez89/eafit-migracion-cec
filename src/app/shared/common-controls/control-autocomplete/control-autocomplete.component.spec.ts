import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlAutocompleteComponent } from './control-autocomplete.component';

describe('ControlAutocompleteComponent', () => {
  let component: ControlAutocompleteComponent;
  let fixture: ComponentFixture<ControlAutocompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlAutocompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlAutocompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
