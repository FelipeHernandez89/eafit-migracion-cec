import { Component, OnInit, Input } from '@angular/core';
import { SpecialFormControl } from '@utilities/SpecialForms';

@Component({
  selector: 'control-read-only',
  templateUrl: './control-read-only.component.html',
  styleUrls: ['./control-read-only.component.scss']
})
export class ControlReadOnlyComponent implements OnInit {
  @Input('sfcontrol') _sfcontrol: SpecialFormControl;

  constructor() { }

  ngOnInit() {
  }

}
