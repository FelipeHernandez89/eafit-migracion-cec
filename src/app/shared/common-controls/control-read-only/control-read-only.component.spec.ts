import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlReadOnlyComponent } from './control-read-only.component';

describe('ControlReadOnlyComponent', () => {
  let component: ControlReadOnlyComponent;
  let fixture: ComponentFixture<ControlReadOnlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlReadOnlyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlReadOnlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
