import { AppContainerComponent } from './app-container/app-container.component';
import { ControlDatepickerComponent } from './common-controls/control-datepicker/control-datepicker.component';
import { ControlTextAreaComponent } from './common-controls/control-textarea/control-textarea.component';
import { ControlInputComponent } from './common-controls/control-input/control-input.component';
import { ControlCheckboxComponent } from './common-controls/control-checkbox/control-checkbox.component';
import { ControlSelectComponent } from './common-controls/control-select/control-select.component';
import { ControlAutocompleteComponent } from './common-controls/control-autocomplete/control-autocomplete.component';
import { AutomaticFormsComponent } from './automatic-forms/automatic-forms.component';
import { ListTableComponent, getCellDataPipe, orderTableColumns } from './list-table/list-table.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { SignUpFormComponent } from './sign-up-form/sign-up-form.component';
import { ControlChipsComponent } from './common-controls/control-chips/control-chips.component';
import { ControlUploadComponent } from './common-controls/control-upload/control-upload.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { ControlMaskedInputComponent } from './common-controls/control-masked-input/control-masked-input.component';
import { ControlReadOnlyComponent } from './common-controls/control-read-only/control-read-only.component';
import { AttachmentComponent } from './attachment/attachment.component';
import { DeleteDialogComponent } from './delete-dialog/delete-dialog.component';
// import { ControlTimepickerComponent } from './common-controls/control-timepicker/control-timepicker.component'
import { IconsByTypePipe } from './attachment/icons-by-type.pipe';

export const components = [
    IconsByTypePipe,
    ControlReadOnlyComponent,
    ControlMaskedInputComponent,
    ConfirmationDialogComponent,
    DeleteDialogComponent,
    SignUpFormComponent,
    LoginFormComponent,
    AttachmentComponent,
    getCellDataPipe,
    orderTableColumns,
    ListTableComponent,
    AutomaticFormsComponent,
    AppContainerComponent,
    ControlDatepickerComponent,
    ControlTextAreaComponent,
    ControlInputComponent,
    ControlCheckboxComponent,
    ControlSelectComponent,
    ControlAutocompleteComponent,
    ControlChipsComponent,
    ControlUploadComponent
]