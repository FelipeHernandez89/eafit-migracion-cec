import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

import { signUpForm } from './sign-up-form.form';
import { fieldDataToArray, SpecialFormGenerator } from '@utilities/SpecialFormGenerator';
import { SpecialFormGroup } from '@utilities/SpecialForms';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sign-up-form',
  templateUrl: './sign-up-form.component.html',
  styleUrls: ['./sign-up-form.component.scss']
})
export class SignUpFormComponent implements OnInit {
  @Output('sendCredentials') sendCredentials: EventEmitter<any> = new EventEmitter();
  @Output('goToLogin') goToLogin: EventEmitter<any> = new EventEmitter();

  @Input('serverError') serverError: string;

  form: SpecialFormGroup;
  formFields=[];

  constructor() { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.formFields = fieldDataToArray(signUpForm());
    this.form = SpecialFormGenerator(this.formFields);
  }

  sendSignUpCredentials() {
    this.sendCredentials.emit(this.form.value);
  }
  onGoToLogin() {
    this.goToLogin.emit();
  }
}
