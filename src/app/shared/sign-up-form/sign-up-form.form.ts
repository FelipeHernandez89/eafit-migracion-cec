import { controlTypes } from '@utilities/SpecialForms';
import { Validators } from '@angular/forms';
export const signUpForm = () => Object.assign({
  firstName: {
    label: "Primer nombre",
    length: 50,
    data:['text',,'accessibility'],
    type: controlTypes.text
  },
  lastName: {
    label: "Apellido",
    type: controlTypes.text,
    length: 50,
    data: ['text'],
  },
  email: {
    label: "E-Mail",
    type: controlTypes.text,
    length: 50,
    data: ['email'],
  },
  Sexo:
  {
    label: "Sexo",
    tooltip:'male or female',
    data: [[{id:1,name:'Hombre'},{id:2,name:'Mujer'}], 'id', 'name','outline','face'],
    type: controlTypes.dropdown
  },
  birth: {
    label: "Fecha nacimiento",
    required:true,
    data: [,,'outline'],
    type: controlTypes.date
  },
  Password: {
    label: "Contraseña",
    type: controlTypes.text,
    length: 50,
    data: ['password'],
  },
  myDescription:{
    label: "mi descripción",
    type: controlTypes.longText,
    data: ['outline','face'],
    length: 200,
  },
  acceptTerms: {
    label: "Acepto los terminos y condiciones",
    type: controlTypes.boolean,
    validations:[Validators.requiredTrue]
  },
  photoURL:{
    label: "Foto",
    required: false,
    type: controlTypes.file,
  },
  test:
  {
    label: "Ciudad autocomplete",
    type:controlTypes.autocomplete,
    data: [
      (query) => './assets/mocks/cities-mock.json',
      (data) => data.map(item => Object.assign({ name: item.nombre, id: item.nombre }))
    ],
  },
});