import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

// Import Modules
import { CoreModule } from './core/core.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '@root/shared/shared.module';
import { ControlAutocompleteComponent } from './shared/common-controls/control-autocomplete/control-autocomplete.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule.forRoot(),
    HttpClientModule,
    BrowserAnimationsModule,
    SharedModule
  ],
  providers: [ControlAutocompleteComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
