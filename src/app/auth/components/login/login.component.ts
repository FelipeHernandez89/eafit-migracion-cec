import { Component, OnInit, AfterContentInit, OnDestroy } from "@angular/core";
import { Subscription } from 'rxjs';

import { UserFacadeService } from '@services/auth/user-facade.service';
import { Router } from '@angular/router';
import { Authenticate } from '@models/Authenticate.model';


@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit, AfterContentInit, OnDestroy {
  error: string = ""
  subs = new Subscription();
  isLoading: boolean = false;

  constructor(
    private userFacadeService: UserFacadeService,
    private router: Router
  ) { }

  ngOnInit() { }

  ngAfterContentInit(): void {
    this.subs.add(
      this.userFacadeService
        .getErrorLogin$()
        .subscribe(error => this.error = error)
    )

    this.subs.add(
      this.userFacadeService
        .isLoadingAuth$()
        .subscribe(isLoading => this.isLoading = isLoading)
    )
  }

  doLogin(credentials: Authenticate) {
    this.userFacadeService
      .loginWithCredential(credentials);
  }

  forgotPass() {
    console.log('I forgot password')
  }

  googleLog() {
    console.log('Google login')
  }

  goSignUp() {
    this.router.navigate(['auth/sign-up'])
  }
  ngOnDestroy(): void {
    if (this.subs) { this.subs.unsubscribe(); }
  }
}
