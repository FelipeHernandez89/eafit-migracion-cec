import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';

export {
    LoginComponent,
    SignUpComponent
};