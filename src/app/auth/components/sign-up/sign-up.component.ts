import { Component, OnInit, AfterContentInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { UserFacadeService } from '@services/auth/user-facade.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit, AfterContentInit, OnDestroy {
  error: string = "";
  isLoading: boolean = false;
  subs = new Subscription();

  constructor(
    private userFacadeService: UserFacadeService,
    private router: Router
  ) { }

  ngOnInit() { }

  ngAfterContentInit(): void {
    this.subs.add(
      this.userFacadeService
        .getErrorLogin$()
        .subscribe(error => this.error = error)
    )

    this.subs.add(
      this.userFacadeService
        .isLoadingAuth$()
        .subscribe(isLoading => this.isLoading = isLoading)
    )
  }

  goToLogin() {
    this.router.navigate(['auth/login'])
  }

  createUser(data: any) {
    const parseData = {
      ...data,
      birth: new Date(data.birth).getTime()
    }
    this.userFacadeService.createUser(parseData)
  }
  ngOnDestroy(): void {
    if (this.subs) { this.subs.unsubscribe(); }
  }
}
