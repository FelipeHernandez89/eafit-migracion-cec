import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import {
  ConfigurationComponent,
  /* DocumentaryComponent, */
  
} from './components/index';
/* import { OrganizationalTypeComponent } from './components/organizational-type/organizational-type.component';
 */

const routes: Routes = [
  {
    path: '',
    component: ConfigurationComponent,
    children: [
      { path: '', redirectTo: 'disposition', pathMatch: 'full' },
      /* { path: 'documentary', component: DocumentaryComponent }, */
      { path: '**', redirectTo: '', pathMatch: 'full' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationRoutingModule { }
