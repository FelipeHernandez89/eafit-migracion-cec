import { Component, OnInit, AfterContentInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { MenuItemModel } from '@models/menu-item.model';
import { UserModel } from '@models/user.model';
import { Router } from '@angular/router';
import { ConfigurationFacadeService } from '@services/configuration/configuration-facade.service';
import { UserFacadeService } from '@services/auth/user-facade.service';
import { emptyUser } from '@utilities/data.util';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit, AfterContentInit, OnDestroy {
  subs = new Subscription();
  menuItems: MenuItemModel[] = []
  navBarOptions: MenuItemModel[] = [];
  currentUser: UserModel = emptyUser()

  constructor(
    private router: Router,
    private configurationFacadeService: ConfigurationFacadeService,
    private userFacadeService: UserFacadeService
  ) { }

  ngOnInit() {
    let dato="es";
    localStorage.setItem("language", dato)
   }

  ngAfterContentInit(): void {
    this.subs.add(
      this.configurationFacadeService
        .getMenuItems$()
        .subscribe(items => this.menuItems = items)
    )
    this.subs.add(
      this.configurationFacadeService
        .getNavBarOptions$()
        .subscribe(items => this.navBarOptions = items)
    )
    this.subs.add(
      this.userFacadeService
        .getCurrentUser$()
        .subscribe(user => {
          console.log("Current user", user);
          this.currentUser = user
        })
    )
  }
  menuAction(menuItem: MenuItemModel) {
    console.log("Menu Item", menuItem);
    if (menuItem.action) {
      menuItem.action();
    }
    if (menuItem.route) {
      this.router.navigate(menuItem.route)
    }
  }
  ngOnDestroy(): void {
    if (this.subs) { this.subs.unsubscribe(); }
  }

}
