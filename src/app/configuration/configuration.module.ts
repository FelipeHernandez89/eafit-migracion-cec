import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfigurationRoutingModule } from './configuration-routing.module';
/* import { DocumentaryComponent } from './components/documentary/documentary.component'; */
import { ConfigurationComponent } from './configuration.component';
import { SharedModule } from '@root/shared/shared.module';
import { PipesModule } from '@root/core/pipes/pipes-module.module';
@NgModule({
  declarations: [
    ConfigurationComponent,
    /* DocumentaryComponent, */
  ],
  imports: [
    PipesModule,
    CommonModule,
    ConfigurationRoutingModule,
    SharedModule
  ]
})
export class ConfigurationModule { }
