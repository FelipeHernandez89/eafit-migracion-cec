import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxDropzoneModule } from 'ngx-dropzone';
import {
  MatFormFieldModule,
  MatTreeModule,
  MatButtonModule,
  MatCheckboxModule,
  MatCardModule,
  MatInputModule,
  MatSlideToggleModule,
  MatDialogModule,
  MatSidenavModule,
  MatStepperModule,
  MatSnackBarModule,
  MatTableModule,
  MatExpansionModule,
  MatDatepickerModule,
  MatIconModule,
  MatSelectModule,
  MatAutocompleteModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatSortModule,
  MatRippleModule,
  MatProgressSpinnerModule,
  MatChipsModule,
} from '@angular/material';
import { TextMaskModule } from 'angular2-text-mask';


const MATERIAL_MODULE = [
  TextMaskModule,
  MatDialogModule, 
  MatFormFieldModule,
  MatButtonModule, 
  MatInputModule,
  NgxDropzoneModule,
  MatRippleModule,
  MatFormFieldModule,
  MatTreeModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatAutocompleteModule,
  MatButtonModule,
  MatCheckboxModule,
  MatCardModule,
  MatInputModule,
  MatSlideToggleModule,
  MatDialogModule,
  MatSidenavModule,
  MatStepperModule,
  MatSnackBarModule,
  MatTableModule,
  MatExpansionModule,
  MatIconModule,
  MatSelectModule,
  MatPaginatorModule,
  MatSortModule,
  MatProgressSpinnerModule,
  MatChipsModule,
]

@NgModule({
  imports: [
    CommonModule,
    ...MATERIAL_MODULE
  ],
  exports: [
    ...MATERIAL_MODULE
  ],
  declarations: []
})
export class CustomMaterialModule { }
