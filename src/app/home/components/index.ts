import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from '../home.component';

export {
    DashboardComponent,
    HomeComponent
};